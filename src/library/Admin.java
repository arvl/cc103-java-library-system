/*
 * The MIT License
 *
 * Copyright 2021 Angelo Gabriel A. Geulin, David Allen Laud, 
 * Ahbby Ghelle A. Tinay, Anne Clarisse H. Gonzales, Kevin Bryan Y. Malabag
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package library;

/**
 *
 * @author arvl
 */
public class Admin extends IdentifiableItem {
    public Admin(String admin_username, String admin_password,
                 String first_name, String last_name,
                 String organization, String email,
                 String contact_number) {
        super(admin_username, admin_password);
        this.first_name = first_name;
        this.last_name = last_name;
        this.organization = organization;
        this.email = email;
        this.contact_number = contact_number;
    }
    
    public String getUsername() {
        return username;
    }
    
    public String getPassword() {
        return password;
    }
    
    public String getFirstName() {
        return first_name;
    }
    
    public String getLastName() {
        return last_name;
    }
    
    public String getOrganization() {
        return organization;
    }
    
    public String getEmail() {
        return email;
    }
    
    public String getContactNumber() {
        return contact_number;
    }
    
    private final String username = getID();
    private final String password = getName();
    private final String first_name;
    private final String last_name;
    private final String organization;
    private final String email;
    private final String contact_number;
}
