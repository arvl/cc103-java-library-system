/*
 * The MIT License
 *
 * Copyright 2021 Angelo Gabriel A. Geulin, David Allen Laud, 
 * Ahbby Ghelle A. Tinay, Anne Clarisse H. Gonzales, Kevin Bryan Y. Malabag
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package library;

/**
 *
 * @author arvl
 */
import java.util.ArrayList;
import java.util.List;

public class Member extends IdentifiableItem {
    public Member(String member_id, String last_name) {
        super(member_id, last_name);
        this.first_name = "";
        this.section = "";
        this.contact_num = "";

        borrowed_books = new ArrayList<>();
    }

    public Member(String member_id, String last_name, String first_name, String section, String contact_num) {
        super(member_id, last_name);
        this.first_name = first_name;
        this.section = section;
        this.contact_num = contact_num;

        borrowed_books = new ArrayList<>();
    }
    
    public String getLastName() {
        return last_name;
    }
    
    public String getFirstName() {
        return first_name;
    }

    public String getSection() {
        return section;
    }

    public String getContactNumber() {
        return contact_num;
    }

    public ArrayList<String> getBorrowedBookIDs() {
        return borrowed_books;
    }

    public int bookCount() {
        return borrowed_books.size();
    }

    public void addBook(String book_id) {
        borrowed_books.add(book_id);
    }

    public void removeBook(String book_id) {
        borrowed_books.remove(book_id);
    }

    public boolean hasBook(String book_id) {
        boolean result = false;
        if (borrowed_books.contains(book_id))
            result = true;

        return result;
    }

    private final String last_name = getName();
    private final String first_name;
    private final String section;
    private final String contact_num;
    private final ArrayList<String> borrowed_books;
}
