/*
 * The MIT License
 *
 * Copyright 2021 Angelo Gabriel A. Geulin, David Allen Laud, 
 * Ahbby Ghelle A. Tinay, Anne Clarisse H. Gonzales, Kevin Bryan Y. Malabag
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package library;

/**
 *
 * @author arvl
 */
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Set;

public class BooksDatabase {
    private static HashMap<String, Book> db_store;
    private static boolean is_initialized = false;
    
    public static void init() {
        db_store = new HashMap<>();
        is_initialized = true;
    }
    
    public static boolean isInitialized() {
        return is_initialized;
    }

    public static void addBook(Book book) {
        db_store.put(book.getID(), book);
    }

    public static Book getBook(String id) {
        return db_store.get(id);
    }
    
    public static void removeBook(String id) {
        db_store.remove(id);
    }
    
    public static Set<String> getAllBookIDs() {
        return db_store.keySet();
    }

    public static boolean isBorrowedBookID(String book_id) {
        boolean result = false;
        if (getBorrowedBookIDs().contains(book_id))
            result = true;

        return result;
    }

    public static boolean isAvailableBookID(String book_id) {
        boolean result = false;
        if (getAvailableBookIDs().contains(book_id))
            result = true;

        return result;
    }

    public static boolean isRegisteredBookID(String book_id) {
        boolean result = false;
        if (db_store.containsKey(book_id))
            result = true;

        return result;
    }

    public static ArrayList<String> getAvailableBookIDs() {
        ArrayList<String> available_book_ids = new ArrayList<>();

        for (Book book : db_store.values()) {
            if (!MembersDatabase.getBookIDsInMembers().contains(book.getID())) {
                available_book_ids.add(book.getID());
            }
        }
        
        return available_book_ids;
    }

    public static ArrayList<String> getBorrowedBookIDs() {
        ArrayList<String> borrowed_book_ids = new ArrayList<>();

        for (Book book : db_store.values()) {
            if (MembersDatabase.getBookIDsInMembers().contains(book.getID())) {
                borrowed_book_ids.add(book.getID());
            }
        }
        
        return borrowed_book_ids;
    }
    
    public static int bookCount() {
        return db_store.size();
    }
}
