/*
 * The MIT License
 *
 * Copyright 2021 Angelo Gabriel A. Geulin, David Allen Laud, 
 * Ahbby Ghelle A. Tinay, Anne Clarisse H. Gonzales, Kevin Bryan Y. Malabag
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package library;

/**
 *
 * @author arvl
 */

import java.util.HashMap;

public class AdminsDatabase {
    private static HashMap<String, Admin> db_store;
    private static boolean is_initialized = false;
    private static boolean is_logged_in = false;
    private static String logged_in_user = "";
    
    public static void init() {
        db_store = new HashMap<>();
        is_initialized = true;
    }
    
    public static boolean isInitialized() {
        return is_initialized;
    }
    
    public static void registerAdmin(Admin admin) {
        // Store admin object with the Username as key.
        db_store.put(admin.getUsername(), admin);
    }
    
    public static Admin getAdmin(String admin_username) {
        return db_store.get(admin_username);
    }
    
    public static boolean adminExists(String admin_username) {
        boolean result = false;
        
        if (db_store.containsKey(admin_username))
            result = true;
        
        return result;
    }
    
    public static boolean isCorrectPassword(String admin_username, String admin_password) {
        boolean result = false;
        
        // Verify passwords using simple plain-text comparison.
        if (adminExists(admin_username) 
            && getAdmin(admin_username).getPassword().equals(admin_password))
            result = true;

        return result;
    }
    
    public static boolean verifyPassword(String admin_username, String admin_password) {
        boolean result = false;
        if (isCorrectPassword(admin_username, admin_password)) {
            result = true;
            is_logged_in = true;
            logged_in_user = admin_username;
        }
        
        return result;
    }
    
    public static boolean isLoggedIn() {
        return is_logged_in;
    }
    
    public static String getLoggedInUser() {
        return logged_in_user;
    }
    
    public static void logOff() {    
        is_logged_in = false;
        logged_in_user = "";
    }
    
    public static void changePassword(String admin_username, String admin_new_password) {
        Admin admin = new Admin(getAdmin(admin_username).getID(), 
                                admin_new_password,
                                getAdmin(admin_username).getFirstName(),
                                getAdmin(admin_username).getLastName(),
                                getAdmin(admin_username).getOrganization(),
                                getAdmin(admin_username).getEmail(),
                                getAdmin(admin_username).getContactNumber());
        
        registerAdmin(admin);
    }
}
