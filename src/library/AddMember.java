/*
 * The MIT License
 *
 * Copyright 2021 Angelo Gabriel A. Geulin, David Allen Laud, 
 * Ahbby Ghelle A. Tinay, Anne Clarisse H. Gonzales, Kevin Bryan Y. Malabag
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package library;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class AddMember extends javax.swing.JFrame {

    public AddMember() {
        initComponents();
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtMemberID = new javax.swing.JTextField();
        txtLastName = new javax.swing.JTextField();
        txtFirstName = new javax.swing.JTextField();
        txtSection = new javax.swing.JTextField();
        txtContactNumber = new javax.swing.JTextField();
        books1 = new javax.swing.JLabel();
        books2 = new javax.swing.JLabel();
        books3 = new javax.swing.JLabel();
        books4 = new javax.swing.JLabel();
        books5 = new javax.swing.JLabel();
        btnInsert = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        btracking_txt = new javax.swing.JLabel();
        bbg3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(630, 350));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(204, 204, 204, 0));
        jPanel1.setMaximumSize(new java.awt.Dimension(630, 350));
        jPanel1.setMinimumSize(new java.awt.Dimension(630, 350));
        jPanel1.setPreferredSize(new java.awt.Dimension(630, 350));

        txtMemberID.setMaximumSize(new java.awt.Dimension(215, 40));
        txtMemberID.setMinimumSize(new java.awt.Dimension(215, 40));
        txtMemberID.setPreferredSize(new java.awt.Dimension(215, 40));

        txtLastName.setMaximumSize(new java.awt.Dimension(215, 40));
        txtLastName.setMinimumSize(new java.awt.Dimension(215, 40));
        txtLastName.setPreferredSize(new java.awt.Dimension(215, 40));

        txtFirstName.setMaximumSize(new java.awt.Dimension(215, 40));
        txtFirstName.setMinimumSize(new java.awt.Dimension(215, 40));
        txtFirstName.setPreferredSize(new java.awt.Dimension(215, 40));

        txtSection.setMaximumSize(new java.awt.Dimension(215, 40));
        txtSection.setMinimumSize(new java.awt.Dimension(215, 40));
        txtSection.setPreferredSize(new java.awt.Dimension(215, 40));

        txtContactNumber.setMaximumSize(new java.awt.Dimension(215, 40));
        txtContactNumber.setMinimumSize(new java.awt.Dimension(215, 40));
        txtContactNumber.setPreferredSize(new java.awt.Dimension(215, 40));

        books1.setFont(new java.awt.Font("Californian FB", 0, 18)); // NOI18N
        books1.setText("Member ID");

        books2.setFont(new java.awt.Font("Californian FB", 0, 18)); // NOI18N
        books2.setText("Last Name");

        books3.setFont(new java.awt.Font("Californian FB", 0, 18)); // NOI18N
        books3.setText("First Name");

        books4.setFont(new java.awt.Font("Californian FB", 0, 18)); // NOI18N
        books4.setText("Section");

        books5.setFont(new java.awt.Font("Californian FB", 0, 18)); // NOI18N
        books5.setText("Contact No.");

        btnInsert.setText("OK");
        btnInsert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInsertActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btracking_txt.setFont(new java.awt.Font("Californian FB", 0, 24)); // NOI18N
        btracking_txt.setText("Add Member");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btracking_txt)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(btnInsert, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(books1)
                                .addComponent(books2)
                                .addComponent(books3)
                                .addComponent(books4)
                                .addComponent(books5))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtMemberID, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtLastName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtFirstName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtSection, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtContactNumber, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(266, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(btracking_txt)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMemberID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(books1))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtLastName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(books2))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtFirstName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(books3))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSection, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(books4))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtContactNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(books5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancel)
                    .addComponent(btnInsert))
                .addContainerGap(33, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        bbg3.setMaximumSize(new java.awt.Dimension(630, 350));
        bbg3.setMinimumSize(new java.awt.Dimension(630, 350));
        bbg3.setPreferredSize(new java.awt.Dimension(630, 350));
        getContentPane().add(bbg3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        goBackToDashboardWindow();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnInsertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInsertActionPerformed
        // Collect data from Swing objects.
        String member_id = txtMemberID.getText();
        String last_name = txtLastName.getText();
        String first_name = txtFirstName.getText();
        String section = txtSection.getText();
        String contact_no = txtContactNumber.getText();
        
        // Input validation.
        if (member_id.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Member ID is required.");
            return;
        }
        
        if (MembersDatabase.isRegisteredMemberID(member_id)) {
            JOptionPane.showMessageDialog(null, "This member ID is already registered.");
            return;
        }
        
        if (last_name.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Last name is required.");
            return;
        }
        
        if (first_name.isEmpty()) {
            JOptionPane.showMessageDialog(null, "First name is required.");
            return;
        }
        
        // Commit transaction.
        Member member = new Member(member_id, last_name, first_name, section, contact_no);
        MembersDatabase.addMember(member);
        
        // Success message.
        JOptionPane.showMessageDialog(null, "New book added.");
        goBackToDashboardWindow();
    }//GEN-LAST:event_btnInsertActionPerformed

    private void goBackToDashboardWindow() {
        Dashboard ai = new Dashboard();
        ai.setVisible(true);
        ai.pack();
        ai.setLocationRelativeTo(null);
        ai.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.dispose();
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AddMember.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AddMember.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AddMember.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AddMember.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AddMember().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel bbg3;
    private javax.swing.JLabel books1;
    private javax.swing.JLabel books2;
    private javax.swing.JLabel books3;
    private javax.swing.JLabel books4;
    private javax.swing.JLabel books5;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnInsert;
    private javax.swing.JLabel btracking_txt;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtContactNumber;
    private javax.swing.JTextField txtFirstName;
    private javax.swing.JTextField txtLastName;
    private javax.swing.JTextField txtMemberID;
    private javax.swing.JTextField txtSection;
    // End of variables declaration//GEN-END:variables
}
