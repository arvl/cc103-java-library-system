/*
 * The MIT License
 *
 * Copyright 2021 Angelo Gabriel A. Geulin, David Allen Laud, 
 * Ahbby Ghelle A. Tinay, Anne Clarisse H. Gonzales, Kevin Bryan Y. Malabag
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package library;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.swing.JTable;
/**
 *
 * @author QCU
 */
public class Dashboard extends javax.swing.JFrame {
    
    private static final String[] books_table_column_names = 
        { "Book ID", "Book Name",
          "Author", "Publisher",
          "Date Published" };
    
    private static final String[] members_table_column_names = 
        { "Member ID", "Last Name",
          "First Name", "Section",
          "Contact No." };
    
    public Dashboard() {
        initComponents();

        showDate();
        showTime();
        
        if (!BooksDatabase.isInitialized()) {
            BooksDatabase.init();
            
            Book noli = new Book("1212", "Noli me Tangere", "Jose Rizal", "IDK", "NO IDEA");
            Book fili = new Book("3434", "El Filibusterismo", "Jose Rizal", "IDK", "NO IDEA");
            Book deka = new Book("7777", "Dekada '70", "Lualhati Bautista", "IDK", "NO IDEA");
            Book desp = new Book("8888", "Desaparesidos", "Lualhati Bautista", "IDK", "NO IDEA");
            Book bang = new Book("9999", "Banaag at Sikat", "Lope K. Santos", "IDK", "NO IDEA");
            Book abnk = new Book("1000", "ABNKKBSNPLko?!", "Bob Ong", "IDK", "NO IDEA");
            
            BooksDatabase.addBook(noli);
            BooksDatabase.addBook(fili);
            BooksDatabase.addBook(deka);
            BooksDatabase.addBook(desp);
            BooksDatabase.addBook(bang);
            BooksDatabase.addBook(abnk);
        }
        
        if (!MembersDatabase.isInitialized()) {
            MembersDatabase.init();
            
            Member angelo = new Member("20-1695", "Geulin", "Angelo Gabriel", "SBIT-1C", "09491393273");
            Member ahbby = new Member("20-1783", "Tinay", "Ahbby Ghelle", "SBIT-1C", "#10");
            
            MembersDatabase.addMember(angelo);
            MembersDatabase.addMember(ahbby);
        }
        
        if (BooksDatabase.bookCount() > 0) {
            reloadBooksTable(tblManageBooks);
        }
        
        if (MembersDatabase.memberCount() > 0) {
            reloadMembersTable(tblManageMembers);
            reloadMembersTable(tblBorrowersTrackingMembers);
        }
        
        tblManageBooks.setSelectionModel(new ForcedListSelectionModel());
        tblManageMembers.setSelectionModel(new ForcedListSelectionModel());
        tblBorrowersTrackingMembers.setSelectionModel(new ForcedListSelectionModel());
        
        lblWelcome.setText("Welcome, " + AdminsDatabase.getAdmin(AdminsDatabase.getLoggedInUser()).getFirstName() + ".");
        lblProgramName.setText(AdminsDatabase.getAdmin(AdminsDatabase.getLoggedInUser()).getOrganization() + " Library System");
    }
    
    private void showDate() {
        Date d = new Date();
        SimpleDateFormat s = new SimpleDateFormat("MM-dd-yyyy");
        date.setText(s.format(d));
    }

    private void showTime() {
            new Timer(0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Date d = new Date();
                SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss a");
                time.setText(s.format(d));
            }
        }
        ).start();
    }
       
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        parent = new javax.swing.JPanel();
        dash = new javax.swing.JPanel();
        btnbtracking = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btnmbook = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        btnmuser = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        btnsetting = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        btnlogout = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        icon_Tracking = new javax.swing.JLabel();
        icon_Mbook = new javax.swing.JLabel();
        icon_Muser = new javax.swing.JLabel();
        icon_Settings = new javax.swing.JLabel();
        icon_logout = new javax.swing.JLabel();
        time = new javax.swing.JLabel();
        date = new javax.swing.JLabel();
        dashBG = new javax.swing.JLabel();
        lblWelcome = new javax.swing.JLabel();
        lblProgramName = new javax.swing.JLabel();
        btmember = new javax.swing.JPanel();
        btracking_txt = new javax.swing.JLabel();
        members_txt = new javax.swing.JLabel();
        btnback = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        btnReturning = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        btnBorrowing = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        membertable = new javax.swing.JScrollPane();
        tblBorrowersTrackingMembers = new javax.swing.JTable();
        bseperator = new javax.swing.JSeparator();
        bbg = new javax.swing.JLabel();
        btreturning = new javax.swing.JPanel();
        btracking1_txt = new javax.swing.JLabel();
        returning_txt = new javax.swing.JLabel();
        btnsearch1 = new javax.swing.JPanel();
        btnback1 = new javax.swing.JPanel();
        jLabel53 = new javax.swing.JLabel();
        btnContinueReturning = new javax.swing.JPanel();
        background1 = new javax.swing.JLabel();
        booktable = new javax.swing.JScrollPane();
        tblBorrowedBooks = new javax.swing.JTable();
        bseperator2 = new javax.swing.JSeparator();
        bbg1 = new javax.swing.JLabel();
        btborrowing = new javax.swing.JPanel();
        btracking1_txt1 = new javax.swing.JLabel();
        books_txt1 = new javax.swing.JLabel();
        btnback9 = new javax.swing.JPanel();
        jLabel54 = new javax.swing.JLabel();
        btnContinueBorrow = new javax.swing.JPanel();
        background3 = new javax.swing.JLabel();
        booktable2 = new javax.swing.JScrollPane();
        tblAvailableBooks = new javax.swing.JTable();
        bseperator9 = new javax.swing.JSeparator();
        bbg9 = new javax.swing.JLabel();
        pnlBTReturningSummary = new javax.swing.JPanel();
        btracking2_txt1 = new javax.swing.JLabel();
        btnsummary1 = new javax.swing.JLabel();
        btnBackToChooseBooksToBorrow = new javax.swing.JPanel();
        jLabel56 = new javax.swing.JLabel();
        membertable2 = new javax.swing.JScrollPane();
        tblReturningMemberSummary = new javax.swing.JTable();
        booktable3 = new javax.swing.JScrollPane();
        tblReturningBooksSummary = new javax.swing.JTable();
        bseperator10 = new javax.swing.JSeparator();
        btnReturnOK = new javax.swing.JPanel();
        background4 = new javax.swing.JLabel();
        bbg10 = new javax.swing.JLabel();
        btracking = new javax.swing.JPanel();
        btracking2_txt = new javax.swing.JLabel();
        btnsummary = new javax.swing.JLabel();
        btnback2 = new javax.swing.JPanel();
        jLabel55 = new javax.swing.JLabel();
        membertable1 = new javax.swing.JScrollPane();
        tblBorrowingMemberSummary = new javax.swing.JTable();
        booktable1 = new javax.swing.JScrollPane();
        tblBorrowingBooksSummary = new javax.swing.JTable();
        bseperator3 = new javax.swing.JSeparator();
        btnBorrowOK = new javax.swing.JPanel();
        background2 = new javax.swing.JLabel();
        bbg2 = new javax.swing.JLabel();
        btnsummary2 = new javax.swing.JLabel();
        mbooks = new javax.swing.JPanel();
        mbooks_txt = new javax.swing.JLabel();
        books1 = new javax.swing.JLabel();
        txtBooksSearchField = new javax.swing.JTextField();
        btnsearch2 = new javax.swing.JPanel();
        jLabel37 = new javax.swing.JLabel();
        btnback3 = new javax.swing.JPanel();
        jLabel57 = new javax.swing.JLabel();
        btnAddBook = new javax.swing.JPanel();
        jLabel62 = new javax.swing.JLabel();
        btnEditBook = new javax.swing.JPanel();
        jLabel64 = new javax.swing.JLabel();
        btnDeleteBook = new javax.swing.JPanel();
        jLabel58 = new javax.swing.JLabel();
        mmembertable1 = new javax.swing.JScrollPane();
        tblManageBooks = new javax.swing.JTable();
        bseperator1 = new javax.swing.JSeparator();
        bbg3 = new javax.swing.JLabel();
        musers = new javax.swing.JPanel();
        muser_txt = new javax.swing.JLabel();
        user_txt = new javax.swing.JLabel();
        txtMembersSearchField = new javax.swing.JTextField();
        btnsearch3 = new javax.swing.JPanel();
        jLabel43 = new javax.swing.JLabel();
        btnback4 = new javax.swing.JPanel();
        jLabel59 = new javax.swing.JLabel();
        btnAddMember = new javax.swing.JPanel();
        jLabel65 = new javax.swing.JLabel();
        btnEditMember = new javax.swing.JPanel();
        jLabel66 = new javax.swing.JLabel();
        btnDeleteMember = new javax.swing.JPanel();
        jLabel67 = new javax.swing.JLabel();
        mmembertable = new javax.swing.JScrollPane();
        tblManageMembers = new javax.swing.JTable();
        bseperator4 = new javax.swing.JSeparator();
        bbg4 = new javax.swing.JLabel();
        settings = new javax.swing.JPanel();
        setting_txt = new javax.swing.JLabel();
        bdatabase_txt = new javax.swing.JLabel();
        mdatabase_txt = new javax.swing.JLabel();
        reset_txt = new javax.swing.JLabel();
        btnbdatabase = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        btnmdatabase = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        btnreset = new javax.swing.JPanel();
        jLabel47 = new javax.swing.JLabel();
        btnback5 = new javax.swing.JPanel();
        jLabel61 = new javax.swing.JLabel();
        bseperator5 = new javax.swing.JSeparator();
        bbg5 = new javax.swing.JLabel();
        bookdatabase = new javax.swing.JPanel();
        bdatabase1_txt = new javax.swing.JLabel();
        btnback6 = new javax.swing.JPanel();
        jLabel63 = new javax.swing.JLabel();
        bseperator6 = new javax.swing.JSeparator();
        bbg6 = new javax.swing.JLabel();
        memberdatabase = new javax.swing.JPanel();
        mdatabase1_txt = new javax.swing.JLabel();
        btnback7 = new javax.swing.JPanel();
        jLabel68 = new javax.swing.JLabel();
        bseperator7 = new javax.swing.JSeparator();
        bbg7 = new javax.swing.JLabel();
        reset = new javax.swing.JPanel();
        reset1_txt = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        changepw_txt = new javax.swing.JLabel();
        currentpw_txt = new javax.swing.JLabel();
        newpw_txt = new javax.swing.JLabel();
        btnResetMembers = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        btnResetBooks = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        changepw_txt1 = new javax.swing.JLabel();
        currentpw_txt1 = new javax.swing.JLabel();
        newpw_txt1 = new javax.swing.JLabel();
        btnChangePassword = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        txtCurrentPassword = new javax.swing.JPasswordField();
        txtNewPassword = new javax.swing.JPasswordField();
        btnback8 = new javax.swing.JPanel();
        jLabel69 = new javax.swing.JLabel();
        bseperator8 = new javax.swing.JSeparator();
        bbg8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1180, 590));
        setResizable(false);

        parent.setMaximumSize(new java.awt.Dimension(1180, 590));
        parent.setMinimumSize(new java.awt.Dimension(1180, 590));
        parent.setLayout(new java.awt.CardLayout());

        dash.setMaximumSize(new java.awt.Dimension(1180, 590));
        dash.setMinimumSize(new java.awt.Dimension(1180, 590));
        dash.setPreferredSize(new java.awt.Dimension(1180, 590));
        dash.setRequestFocusEnabled(false);
        dash.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnbtracking.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnbtracking.setMaximumSize(new java.awt.Dimension(200, 130));
        btnbtracking.setMinimumSize(new java.awt.Dimension(200, 130));
        btnbtracking.setPreferredSize(new java.awt.Dimension(200, 130));
        btnbtracking.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnbtrackingMouseClicked(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/icons8_user_rights_filled_100px.png"))); // NOI18N

        javax.swing.GroupLayout btnbtrackingLayout = new javax.swing.GroupLayout(btnbtracking);
        btnbtracking.setLayout(btnbtrackingLayout);
        btnbtrackingLayout.setHorizontalGroup(
            btnbtrackingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnbtrackingLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        btnbtrackingLayout.setVerticalGroup(
            btnbtrackingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnbtrackingLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
                .addContainerGap())
        );

        dash.add(btnbtracking, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 156, -1, -1));

        btnmbook.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnmbook.setMaximumSize(new java.awt.Dimension(200, 130));
        btnmbook.setMinimumSize(new java.awt.Dimension(200, 130));
        btnmbook.setPreferredSize(new java.awt.Dimension(200, 130));
        btnmbook.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnmbookMouseClicked(evt);
            }
        });

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/icons8_books_100px_1.png"))); // NOI18N

        javax.swing.GroupLayout btnmbookLayout = new javax.swing.GroupLayout(btnmbook);
        btnmbook.setLayout(btnmbookLayout);
        btnmbookLayout.setHorizontalGroup(
            btnmbookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnmbookLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(jLabel5)
                .addContainerGap(50, Short.MAX_VALUE))
        );
        btnmbookLayout.setVerticalGroup(
            btnmbookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, btnmbookLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
                .addContainerGap())
        );

        dash.add(btnmbook, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 156, -1, -1));

        btnmuser.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnmuser.setMaximumSize(new java.awt.Dimension(200, 130));
        btnmuser.setMinimumSize(new java.awt.Dimension(200, 130));
        btnmuser.setPreferredSize(new java.awt.Dimension(200, 130));
        btnmuser.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnmuserMouseClicked(evt);
            }
        });

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/icons8_edit_user_100px.png"))); // NOI18N

        javax.swing.GroupLayout btnmuserLayout = new javax.swing.GroupLayout(btnmuser);
        btnmuser.setLayout(btnmuserLayout);
        btnmuserLayout.setHorizontalGroup(
            btnmuserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnmuserLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(jLabel4)
                .addContainerGap(50, Short.MAX_VALUE))
        );
        btnmuserLayout.setVerticalGroup(
            btnmuserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnmuserLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
                .addContainerGap())
        );

        dash.add(btnmuser, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 156, -1, -1));

        btnsetting.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnsetting.setMaximumSize(new java.awt.Dimension(200, 130));
        btnsetting.setMinimumSize(new java.awt.Dimension(200, 130));
        btnsetting.setPreferredSize(new java.awt.Dimension(200, 130));
        btnsetting.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnsettingMouseClicked(evt);
            }
        });

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/icons8_settings_100px_1.png"))); // NOI18N

        javax.swing.GroupLayout btnsettingLayout = new javax.swing.GroupLayout(btnsetting);
        btnsetting.setLayout(btnsettingLayout);
        btnsettingLayout.setHorizontalGroup(
            btnsettingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnsettingLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(jLabel2)
                .addContainerGap(50, Short.MAX_VALUE))
        );
        btnsettingLayout.setVerticalGroup(
            btnsettingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnsettingLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
                .addContainerGap())
        );

        dash.add(btnsetting, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 156, -1, -1));

        btnlogout.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnlogout.setMaximumSize(new java.awt.Dimension(200, 130));
        btnlogout.setMinimumSize(new java.awt.Dimension(200, 130));
        btnlogout.setPreferredSize(new java.awt.Dimension(200, 130));
        btnlogout.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnlogoutMouseClicked(evt);
            }
        });

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/icons8_shutdown_100px.png"))); // NOI18N

        javax.swing.GroupLayout btnlogoutLayout = new javax.swing.GroupLayout(btnlogout);
        btnlogout.setLayout(btnlogoutLayout);
        btnlogoutLayout.setHorizontalGroup(
            btnlogoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnlogoutLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(jLabel3)
                .addContainerGap(50, Short.MAX_VALUE))
        );
        btnlogoutLayout.setVerticalGroup(
            btnlogoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnlogoutLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        dash.add(btnlogout, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 156, -1, -1));

        icon_Tracking.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        icon_Tracking.setText("   Borrowers Tracking");
        icon_Tracking.setMaximumSize(new java.awt.Dimension(200, 35));
        icon_Tracking.setMinimumSize(new java.awt.Dimension(200, 35));
        icon_Tracking.setPreferredSize(new java.awt.Dimension(200, 35));
        dash.add(icon_Tracking, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 290, 210, -1));

        icon_Mbook.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        icon_Mbook.setText("        Manage Book");
        icon_Mbook.setMaximumSize(new java.awt.Dimension(200, 35));
        icon_Mbook.setMinimumSize(new java.awt.Dimension(200, 35));
        icon_Mbook.setPreferredSize(new java.awt.Dimension(200, 35));
        dash.add(icon_Mbook, new org.netbeans.lib.awtextra.AbsoluteConstraints(268, 292, -1, -1));

        icon_Muser.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        icon_Muser.setText("        Manage User");
        icon_Muser.setMaximumSize(new java.awt.Dimension(200, 35));
        icon_Muser.setMinimumSize(new java.awt.Dimension(200, 35));
        icon_Muser.setPreferredSize(new java.awt.Dimension(200, 35));
        dash.add(icon_Muser, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 292, -1, -1));

        icon_Settings.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        icon_Settings.setText("             Settings");
        icon_Settings.setMaximumSize(new java.awt.Dimension(200, 35));
        icon_Settings.setMinimumSize(new java.awt.Dimension(200, 35));
        icon_Settings.setPreferredSize(new java.awt.Dimension(200, 35));
        dash.add(icon_Settings, new org.netbeans.lib.awtextra.AbsoluteConstraints(708, 292, -1, -1));

        icon_logout.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        icon_logout.setText("              Logout");
        icon_logout.setMaximumSize(new java.awt.Dimension(200, 35));
        icon_logout.setMinimumSize(new java.awt.Dimension(200, 35));
        icon_logout.setPreferredSize(new java.awt.Dimension(200, 35));
        dash.add(icon_logout, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 292, -1, -1));

        time.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        time.setText("TIME");
        dash.add(time, new org.netbeans.lib.awtextra.AbsoluteConstraints(1050, 510, -1, -1));

        date.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        date.setText("DATE");
        dash.add(date, new org.netbeans.lib.awtextra.AbsoluteConstraints(1050, 550, -1, -1));
        dash.add(dashBG, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        lblWelcome.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        lblWelcome.setText("Welcome.");
        dash.add(lblWelcome, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 490, -1, -1));

        lblProgramName.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        lblProgramName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblProgramName.setText("Library System");
        dash.add(lblProgramName, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 47, 1080, 50));

        parent.add(dash, "card2");

        btmember.setBackground(new java.awt.Color(51, 255, 255));
        btmember.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btracking_txt.setFont(new java.awt.Font("Californian FB", 0, 24)); // NOI18N
        btracking_txt.setText("Borrower's Tracking");
        btmember.add(btracking_txt, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 20, -1, -1));

        members_txt.setFont(new java.awt.Font("Californian FB", 0, 18)); // NOI18N
        members_txt.setText("Choose a member:");
        btmember.add(members_txt, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 70, -1, -1));

        btnback.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnback.setMaximumSize(new java.awt.Dimension(150, 45));
        btnback.setMinimumSize(new java.awt.Dimension(150, 45));
        btnback.setPreferredSize(new java.awt.Dimension(150, 45));
        btnback.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnbackMouseClicked(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel15.setText("BACK");

        javax.swing.GroupLayout btnbackLayout = new javax.swing.GroupLayout(btnback);
        btnback.setLayout(btnbackLayout);
        btnbackLayout.setHorizontalGroup(
            btnbackLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnbackLayout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jLabel15)
                .addContainerGap(49, Short.MAX_VALUE))
        );
        btnbackLayout.setVerticalGroup(
            btnbackLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnbackLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        btmember.add(btnback, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 510, -1, -1));

        btnReturning.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnReturning.setMaximumSize(new java.awt.Dimension(150, 45));
        btnReturning.setMinimumSize(new java.awt.Dimension(150, 45));
        btnReturning.setPreferredSize(new java.awt.Dimension(150, 45));
        btnReturning.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnReturningMouseClicked(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Californian FB", 1, 18)); // NOI18N
        jLabel20.setText("Returning Book");

        javax.swing.GroupLayout btnReturningLayout = new javax.swing.GroupLayout(btnReturning);
        btnReturning.setLayout(btnReturningLayout);
        btnReturningLayout.setHorizontalGroup(
            btnReturningLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnReturningLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel20)
                .addContainerGap(17, Short.MAX_VALUE))
        );
        btnReturningLayout.setVerticalGroup(
            btnReturningLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnReturningLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        btmember.add(btnReturning, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 510, 170, -1));

        btnBorrowing.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnBorrowing.setMaximumSize(new java.awt.Dimension(150, 45));
        btnBorrowing.setMinimumSize(new java.awt.Dimension(150, 45));
        btnBorrowing.setPreferredSize(new java.awt.Dimension(150, 45));
        btnBorrowing.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnBorrowingMouseClicked(evt);
            }
        });

        jLabel22.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel22.setText("Borrowing Book");

        javax.swing.GroupLayout btnBorrowingLayout = new javax.swing.GroupLayout(btnBorrowing);
        btnBorrowing.setLayout(btnBorrowingLayout);
        btnBorrowingLayout.setHorizontalGroup(
            btnBorrowingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnBorrowingLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        btnBorrowingLayout.setVerticalGroup(
            btnBorrowingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnBorrowingLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        btmember.add(btnBorrowing, new org.netbeans.lib.awtextra.AbsoluteConstraints(910, 510, 180, -1));

        tblBorrowersTrackingMembers.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        membertable.setViewportView(tblBorrowersTrackingMembers);

        btmember.add(membertable, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 100, 1020, 380));

        bseperator.setBackground(new java.awt.Color(0, 0, 0));
        bseperator.setForeground(new java.awt.Color(0, 0, 0));
        bseperator.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        btmember.add(bseperator, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 560, 1020, -1));
        btmember.add(bbg, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        parent.add(btmember, "card3");

        btreturning.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btracking1_txt.setFont(new java.awt.Font("Californian FB", 0, 24)); // NOI18N
        btracking1_txt.setText("Borrower`s Tracking");
        btreturning.add(btracking1_txt, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 20, -1, -1));

        returning_txt.setFont(new java.awt.Font("Californian FB", 0, 18)); // NOI18N
        returning_txt.setText("Borrowed Books");
        btreturning.add(returning_txt, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 70, -1, -1));

        btnsearch1.setMaximumSize(new java.awt.Dimension(50, 35));
        btnsearch1.setMinimumSize(new java.awt.Dimension(50, 35));

        javax.swing.GroupLayout btnsearch1Layout = new javax.swing.GroupLayout(btnsearch1);
        btnsearch1.setLayout(btnsearch1Layout);
        btnsearch1Layout.setHorizontalGroup(
            btnsearch1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 59, Short.MAX_VALUE)
        );
        btnsearch1Layout.setVerticalGroup(
            btnsearch1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );

        btreturning.add(btnsearch1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 50, -1, 40));

        btnback1.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnback1.setMaximumSize(new java.awt.Dimension(150, 45));
        btnback1.setMinimumSize(new java.awt.Dimension(150, 45));
        btnback1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnback1MouseClicked(evt);
            }
        });

        jLabel53.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel53.setText("BACK");

        javax.swing.GroupLayout btnback1Layout = new javax.swing.GroupLayout(btnback1);
        btnback1.setLayout(btnback1Layout);
        btnback1Layout.setHorizontalGroup(
            btnback1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnback1Layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jLabel53)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        btnback1Layout.setVerticalGroup(
            btnback1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnback1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel53, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        btreturning.add(btnback1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 510, -1, -1));

        btnContinueReturning.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnContinueReturning.setMaximumSize(new java.awt.Dimension(150, 45));
        btnContinueReturning.setMinimumSize(new java.awt.Dimension(150, 45));
        btnContinueReturning.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnContinueReturningMouseClicked(evt);
            }
        });

        background1.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        background1.setText(" CONTINUE");

        javax.swing.GroupLayout btnContinueReturningLayout = new javax.swing.GroupLayout(btnContinueReturning);
        btnContinueReturning.setLayout(btnContinueReturningLayout);
        btnContinueReturningLayout.setHorizontalGroup(
            btnContinueReturningLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnContinueReturningLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(background1, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                .addContainerGap())
        );
        btnContinueReturningLayout.setVerticalGroup(
            btnContinueReturningLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnContinueReturningLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(background1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        btreturning.add(btnContinueReturning, new org.netbeans.lib.awtextra.AbsoluteConstraints(940, 510, -1, -1));

        tblBorrowedBooks.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        booktable.setViewportView(tblBorrowedBooks);

        btreturning.add(booktable, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 100, 1020, 380));

        bseperator2.setBackground(new java.awt.Color(0, 0, 0));
        bseperator2.setForeground(new java.awt.Color(0, 0, 0));
        bseperator2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        btreturning.add(bseperator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 560, 1020, -1));
        btreturning.add(bbg1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        parent.add(btreturning, "card7");

        btborrowing.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btracking1_txt1.setFont(new java.awt.Font("Californian FB", 0, 24)); // NOI18N
        btracking1_txt1.setText("Borrower`s Tracking");
        btborrowing.add(btracking1_txt1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 20, -1, -1));

        books_txt1.setFont(new java.awt.Font("Californian FB", 0, 18)); // NOI18N
        books_txt1.setText("Available Books");
        btborrowing.add(books_txt1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 70, -1, -1));

        btnback9.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnback9.setMaximumSize(new java.awt.Dimension(150, 45));
        btnback9.setMinimumSize(new java.awt.Dimension(150, 45));
        btnback9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnback9MouseClicked(evt);
            }
        });

        jLabel54.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel54.setText("BACK");

        javax.swing.GroupLayout btnback9Layout = new javax.swing.GroupLayout(btnback9);
        btnback9.setLayout(btnback9Layout);
        btnback9Layout.setHorizontalGroup(
            btnback9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnback9Layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jLabel54)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        btnback9Layout.setVerticalGroup(
            btnback9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnback9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel54, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        btborrowing.add(btnback9, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 510, -1, -1));

        btnContinueBorrow.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnContinueBorrow.setMaximumSize(new java.awt.Dimension(150, 45));
        btnContinueBorrow.setMinimumSize(new java.awt.Dimension(150, 45));
        btnContinueBorrow.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnContinueBorrowMouseClicked(evt);
            }
        });

        background3.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        background3.setText("Next");

        javax.swing.GroupLayout btnContinueBorrowLayout = new javax.swing.GroupLayout(btnContinueBorrow);
        btnContinueBorrow.setLayout(btnContinueBorrowLayout);
        btnContinueBorrowLayout.setHorizontalGroup(
            btnContinueBorrowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnContinueBorrowLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(background3, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                .addContainerGap())
        );
        btnContinueBorrowLayout.setVerticalGroup(
            btnContinueBorrowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnContinueBorrowLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(background3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        btborrowing.add(btnContinueBorrow, new org.netbeans.lib.awtextra.AbsoluteConstraints(940, 510, -1, -1));

        tblAvailableBooks.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        booktable2.setViewportView(tblAvailableBooks);

        btborrowing.add(booktable2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 100, 1020, 380));

        bseperator9.setBackground(new java.awt.Color(0, 0, 0));
        bseperator9.setForeground(new java.awt.Color(0, 0, 0));
        bseperator9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        btborrowing.add(bseperator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 560, 1020, -1));
        btborrowing.add(bbg9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        parent.add(btborrowing, "card12");

        pnlBTReturningSummary.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btracking2_txt1.setFont(new java.awt.Font("Californian FB", 0, 24)); // NOI18N
        btracking2_txt1.setText("Borrower`s Tracking");
        pnlBTReturningSummary.add(btracking2_txt1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 20, -1, -1));

        btnsummary1.setFont(new java.awt.Font("Californian FB", 0, 18)); // NOI18N
        btnsummary1.setText("MEMBERS");
        pnlBTReturningSummary.add(btnsummary1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 70, -1, -1));

        btnBackToChooseBooksToBorrow.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnBackToChooseBooksToBorrow.setMaximumSize(new java.awt.Dimension(150, 45));
        btnBackToChooseBooksToBorrow.setMinimumSize(new java.awt.Dimension(150, 45));
        btnBackToChooseBooksToBorrow.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnBackToChooseBooksToBorrowMouseClicked(evt);
            }
        });

        jLabel56.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel56.setText("BACK");

        javax.swing.GroupLayout btnBackToChooseBooksToBorrowLayout = new javax.swing.GroupLayout(btnBackToChooseBooksToBorrow);
        btnBackToChooseBooksToBorrow.setLayout(btnBackToChooseBooksToBorrowLayout);
        btnBackToChooseBooksToBorrowLayout.setHorizontalGroup(
            btnBackToChooseBooksToBorrowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnBackToChooseBooksToBorrowLayout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jLabel56)
                .addContainerGap(49, Short.MAX_VALUE))
        );
        btnBackToChooseBooksToBorrowLayout.setVerticalGroup(
            btnBackToChooseBooksToBorrowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnBackToChooseBooksToBorrowLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel56, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlBTReturningSummary.add(btnBackToChooseBooksToBorrow, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 510, -1, -1));

        tblReturningMemberSummary.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        membertable2.setViewportView(tblReturningMemberSummary);

        pnlBTReturningSummary.add(membertable2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 100, 470, 380));

        tblReturningBooksSummary.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        booktable3.setViewportView(tblReturningBooksSummary);

        pnlBTReturningSummary.add(booktable3, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 100, 470, 380));

        bseperator10.setBackground(new java.awt.Color(0, 0, 0));
        bseperator10.setForeground(new java.awt.Color(0, 0, 0));
        bseperator10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pnlBTReturningSummary.add(bseperator10, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 560, 1020, -1));

        btnReturnOK.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnReturnOK.setMaximumSize(new java.awt.Dimension(150, 45));
        btnReturnOK.setMinimumSize(new java.awt.Dimension(150, 45));
        btnReturnOK.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnReturnOKMouseClicked(evt);
            }
        });

        background4.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        background4.setText("OK");

        javax.swing.GroupLayout btnReturnOKLayout = new javax.swing.GroupLayout(btnReturnOK);
        btnReturnOK.setLayout(btnReturnOKLayout);
        btnReturnOKLayout.setHorizontalGroup(
            btnReturnOKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnReturnOKLayout.createSequentialGroup()
                .addGap(60, 60, 60)
                .addComponent(background4)
                .addContainerGap(61, Short.MAX_VALUE))
        );
        btnReturnOKLayout.setVerticalGroup(
            btnReturnOKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnReturnOKLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(background4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlBTReturningSummary.add(btnReturnOK, new org.netbeans.lib.awtextra.AbsoluteConstraints(940, 510, -1, -1));
        pnlBTReturningSummary.add(bbg10, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        parent.add(pnlBTReturningSummary, "card8");

        btracking.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btracking2_txt.setFont(new java.awt.Font("Californian FB", 0, 24)); // NOI18N
        btracking2_txt.setText("Borrower`s Tracking");
        btracking.add(btracking2_txt, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 20, -1, -1));

        btnsummary.setFont(new java.awt.Font("Californian FB", 0, 18)); // NOI18N
        btnsummary.setText("BOOKS TO BORROW");
        btracking.add(btnsummary, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 70, -1, -1));

        btnback2.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnback2.setMaximumSize(new java.awt.Dimension(150, 45));
        btnback2.setMinimumSize(new java.awt.Dimension(150, 45));
        btnback2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnback2MouseClicked(evt);
            }
        });

        jLabel55.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel55.setText("BACK");

        javax.swing.GroupLayout btnback2Layout = new javax.swing.GroupLayout(btnback2);
        btnback2.setLayout(btnback2Layout);
        btnback2Layout.setHorizontalGroup(
            btnback2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnback2Layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jLabel55)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        btnback2Layout.setVerticalGroup(
            btnback2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnback2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel55, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        btracking.add(btnback2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 510, -1, -1));

        tblBorrowingMemberSummary.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        membertable1.setViewportView(tblBorrowingMemberSummary);

        btracking.add(membertable1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 100, 470, 380));

        tblBorrowingBooksSummary.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        booktable1.setViewportView(tblBorrowingBooksSummary);

        btracking.add(booktable1, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 100, 470, 380));

        bseperator3.setBackground(new java.awt.Color(0, 0, 0));
        bseperator3.setForeground(new java.awt.Color(0, 0, 0));
        bseperator3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        btracking.add(bseperator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 560, 1020, -1));

        btnBorrowOK.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnBorrowOK.setMaximumSize(new java.awt.Dimension(150, 45));
        btnBorrowOK.setMinimumSize(new java.awt.Dimension(150, 45));
        btnBorrowOK.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnBorrowOKMouseClicked(evt);
            }
        });

        background2.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        background2.setText("OK");

        javax.swing.GroupLayout btnBorrowOKLayout = new javax.swing.GroupLayout(btnBorrowOK);
        btnBorrowOK.setLayout(btnBorrowOKLayout);
        btnBorrowOKLayout.setHorizontalGroup(
            btnBorrowOKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnBorrowOKLayout.createSequentialGroup()
                .addGap(60, 60, 60)
                .addComponent(background2)
                .addContainerGap(60, Short.MAX_VALUE))
        );
        btnBorrowOKLayout.setVerticalGroup(
            btnBorrowOKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnBorrowOKLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(background2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        btracking.add(btnBorrowOK, new org.netbeans.lib.awtextra.AbsoluteConstraints(940, 510, -1, -1));
        btracking.add(bbg2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        btnsummary2.setFont(new java.awt.Font("Californian FB", 0, 18)); // NOI18N
        btnsummary2.setText("MEMBER");
        btracking.add(btnsummary2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 70, -1, -1));

        parent.add(btracking, "card8");

        mbooks.setBackground(new java.awt.Color(255, 255, 255));
        mbooks.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        mbooks_txt.setFont(new java.awt.Font("Californian FB", 0, 24)); // NOI18N
        mbooks_txt.setText("Manage Books");
        mbooks.add(mbooks_txt, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 20, -1, -1));

        books1.setFont(new java.awt.Font("Californian FB", 0, 18)); // NOI18N
        books1.setText("BOOKS");
        mbooks.add(books1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 70, -1, -1));

        txtBooksSearchField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBooksSearchFieldKeyTyped(evt);
            }
        });
        mbooks.add(txtBooksSearchField, new org.netbeans.lib.awtextra.AbsoluteConstraints(869, 50, 170, 40));

        btnsearch2.setMaximumSize(new java.awt.Dimension(50, 35));
        btnsearch2.setMinimumSize(new java.awt.Dimension(50, 35));

        jLabel37.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/icons8_search_35px_1.png"))); // NOI18N

        javax.swing.GroupLayout btnsearch2Layout = new javax.swing.GroupLayout(btnsearch2);
        btnsearch2.setLayout(btnsearch2Layout);
        btnsearch2Layout.setHorizontalGroup(
            btnsearch2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, btnsearch2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel37)
                .addContainerGap())
        );
        btnsearch2Layout.setVerticalGroup(
            btnsearch2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel37, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        mbooks.add(btnsearch2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 50, -1, 40));

        btnback3.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnback3.setMaximumSize(new java.awt.Dimension(150, 45));
        btnback3.setMinimumSize(new java.awt.Dimension(150, 45));
        btnback3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnback3MouseClicked(evt);
            }
        });

        jLabel57.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel57.setText("BACK");

        javax.swing.GroupLayout btnback3Layout = new javax.swing.GroupLayout(btnback3);
        btnback3.setLayout(btnback3Layout);
        btnback3Layout.setHorizontalGroup(
            btnback3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnback3Layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jLabel57)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        btnback3Layout.setVerticalGroup(
            btnback3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnback3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel57, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        mbooks.add(btnback3, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 510, -1, -1));

        btnAddBook.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnAddBook.setMaximumSize(new java.awt.Dimension(150, 45));
        btnAddBook.setMinimumSize(new java.awt.Dimension(150, 45));
        btnAddBook.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAddBookMouseClicked(evt);
            }
        });

        jLabel62.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel62.setText("ADD");

        javax.swing.GroupLayout btnAddBookLayout = new javax.swing.GroupLayout(btnAddBook);
        btnAddBook.setLayout(btnAddBookLayout);
        btnAddBookLayout.setHorizontalGroup(
            btnAddBookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnAddBookLayout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(jLabel62)
                .addContainerGap(53, Short.MAX_VALUE))
        );
        btnAddBookLayout.setVerticalGroup(
            btnAddBookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnAddBookLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel62, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        mbooks.add(btnAddBook, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 510, -1, -1));

        btnEditBook.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnEditBook.setMaximumSize(new java.awt.Dimension(150, 45));
        btnEditBook.setMinimumSize(new java.awt.Dimension(150, 45));
        btnEditBook.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnEditBookMouseClicked(evt);
            }
        });

        jLabel64.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel64.setText("EDIT");

        javax.swing.GroupLayout btnEditBookLayout = new javax.swing.GroupLayout(btnEditBook);
        btnEditBook.setLayout(btnEditBookLayout);
        btnEditBookLayout.setHorizontalGroup(
            btnEditBookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnEditBookLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(jLabel64)
                .addContainerGap(51, Short.MAX_VALUE))
        );
        btnEditBookLayout.setVerticalGroup(
            btnEditBookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnEditBookLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel64, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        mbooks.add(btnEditBook, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 510, -1, -1));

        btnDeleteBook.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnDeleteBook.setMaximumSize(new java.awt.Dimension(150, 45));
        btnDeleteBook.setMinimumSize(new java.awt.Dimension(150, 45));
        btnDeleteBook.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnDeleteBookMouseClicked(evt);
            }
        });

        jLabel58.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel58.setText("DELETE");

        javax.swing.GroupLayout btnDeleteBookLayout = new javax.swing.GroupLayout(btnDeleteBook);
        btnDeleteBook.setLayout(btnDeleteBookLayout);
        btnDeleteBookLayout.setHorizontalGroup(
            btnDeleteBookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnDeleteBookLayout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(jLabel58)
                .addContainerGap(35, Short.MAX_VALUE))
        );
        btnDeleteBookLayout.setVerticalGroup(
            btnDeleteBookLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnDeleteBookLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel58, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        mbooks.add(btnDeleteBook, new org.netbeans.lib.awtextra.AbsoluteConstraints(940, 510, 150, -1));

        tblManageBooks.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tblManageBooks.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblManageBooksMouseClicked(evt);
            }
        });
        mmembertable1.setViewportView(tblManageBooks);

        mbooks.add(mmembertable1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 100, 1020, 380));

        bseperator1.setBackground(new java.awt.Color(0, 0, 0));
        bseperator1.setForeground(new java.awt.Color(0, 0, 0));
        bseperator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        mbooks.add(bseperator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 560, 1020, -1));
        mbooks.add(bbg3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        parent.add(mbooks, "card4");

        musers.setBackground(new java.awt.Color(204, 0, 0));
        musers.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        muser_txt.setFont(new java.awt.Font("Californian FB", 0, 24)); // NOI18N
        muser_txt.setText("Manage Users");
        musers.add(muser_txt, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 20, -1, -1));

        user_txt.setFont(new java.awt.Font("Californian FB", 0, 18)); // NOI18N
        user_txt.setText("MANAGE MEMBER");
        musers.add(user_txt, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 70, 170, -1));

        txtMembersSearchField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtMembersSearchFieldKeyTyped(evt);
            }
        });
        musers.add(txtMembersSearchField, new org.netbeans.lib.awtextra.AbsoluteConstraints(869, 50, 170, 40));

        btnsearch3.setMaximumSize(new java.awt.Dimension(50, 35));
        btnsearch3.setMinimumSize(new java.awt.Dimension(50, 35));

        jLabel43.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/icons8_search_35px_1.png"))); // NOI18N

        javax.swing.GroupLayout btnsearch3Layout = new javax.swing.GroupLayout(btnsearch3);
        btnsearch3.setLayout(btnsearch3Layout);
        btnsearch3Layout.setHorizontalGroup(
            btnsearch3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, btnsearch3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel43)
                .addContainerGap())
        );
        btnsearch3Layout.setVerticalGroup(
            btnsearch3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel43, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        musers.add(btnsearch3, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 50, -1, 40));

        btnback4.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnback4.setMaximumSize(new java.awt.Dimension(150, 45));
        btnback4.setMinimumSize(new java.awt.Dimension(150, 45));
        btnback4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnback4MouseClicked(evt);
            }
        });

        jLabel59.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel59.setText("BACK");

        javax.swing.GroupLayout btnback4Layout = new javax.swing.GroupLayout(btnback4);
        btnback4.setLayout(btnback4Layout);
        btnback4Layout.setHorizontalGroup(
            btnback4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnback4Layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jLabel59)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        btnback4Layout.setVerticalGroup(
            btnback4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnback4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel59, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        musers.add(btnback4, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 510, -1, -1));

        btnAddMember.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnAddMember.setMaximumSize(new java.awt.Dimension(150, 45));
        btnAddMember.setMinimumSize(new java.awt.Dimension(150, 45));
        btnAddMember.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAddMemberMouseClicked(evt);
            }
        });

        jLabel65.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel65.setText("ADD");

        javax.swing.GroupLayout btnAddMemberLayout = new javax.swing.GroupLayout(btnAddMember);
        btnAddMember.setLayout(btnAddMemberLayout);
        btnAddMemberLayout.setHorizontalGroup(
            btnAddMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnAddMemberLayout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(jLabel65)
                .addContainerGap(53, Short.MAX_VALUE))
        );
        btnAddMemberLayout.setVerticalGroup(
            btnAddMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnAddMemberLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel65, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        musers.add(btnAddMember, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 510, -1, -1));

        btnEditMember.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnEditMember.setMaximumSize(new java.awt.Dimension(150, 45));
        btnEditMember.setMinimumSize(new java.awt.Dimension(150, 45));
        btnEditMember.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnEditMemberMouseClicked(evt);
            }
        });

        jLabel66.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel66.setText("EDIT");

        javax.swing.GroupLayout btnEditMemberLayout = new javax.swing.GroupLayout(btnEditMember);
        btnEditMember.setLayout(btnEditMemberLayout);
        btnEditMemberLayout.setHorizontalGroup(
            btnEditMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnEditMemberLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(jLabel66)
                .addContainerGap(51, Short.MAX_VALUE))
        );
        btnEditMemberLayout.setVerticalGroup(
            btnEditMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnEditMemberLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel66, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        musers.add(btnEditMember, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 510, -1, -1));

        btnDeleteMember.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnDeleteMember.setMaximumSize(new java.awt.Dimension(150, 45));
        btnDeleteMember.setMinimumSize(new java.awt.Dimension(150, 45));
        btnDeleteMember.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnDeleteMemberMouseClicked(evt);
            }
        });

        jLabel67.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel67.setText("DELETE");

        javax.swing.GroupLayout btnDeleteMemberLayout = new javax.swing.GroupLayout(btnDeleteMember);
        btnDeleteMember.setLayout(btnDeleteMemberLayout);
        btnDeleteMemberLayout.setHorizontalGroup(
            btnDeleteMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnDeleteMemberLayout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(jLabel67)
                .addContainerGap(35, Short.MAX_VALUE))
        );
        btnDeleteMemberLayout.setVerticalGroup(
            btnDeleteMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnDeleteMemberLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel67, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        musers.add(btnDeleteMember, new org.netbeans.lib.awtextra.AbsoluteConstraints(940, 510, -1, -1));

        tblManageMembers.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tblManageMembers.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblManageMembersMouseClicked(evt);
            }
        });
        mmembertable.setViewportView(tblManageMembers);

        musers.add(mmembertable, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 100, 1020, 380));

        bseperator4.setBackground(new java.awt.Color(0, 0, 0));
        bseperator4.setForeground(new java.awt.Color(0, 0, 0));
        bseperator4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        musers.add(bseperator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 560, 1020, -1));
        musers.add(bbg4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        parent.add(musers, "card5");

        settings.setBackground(new java.awt.Color(51, 255, 51));
        settings.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        setting_txt.setFont(new java.awt.Font("Californian FB", 0, 24)); // NOI18N
        setting_txt.setText("Setting");
        settings.add(setting_txt, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 20, -1, -1));

        bdatabase_txt.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        bdatabase_txt.setText("       Books DataBase");
        bdatabase_txt.setMaximumSize(new java.awt.Dimension(200, 35));
        bdatabase_txt.setMinimumSize(new java.awt.Dimension(200, 35));
        bdatabase_txt.setPreferredSize(new java.awt.Dimension(200, 35));
        settings.add(bdatabase_txt, new org.netbeans.lib.awtextra.AbsoluteConstraints(268, 292, -1, -1));

        mdatabase_txt.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        mdatabase_txt.setText(" Memebers DataBase");
        mdatabase_txt.setMaximumSize(new java.awt.Dimension(200, 35));
        mdatabase_txt.setMinimumSize(new java.awt.Dimension(200, 35));
        mdatabase_txt.setPreferredSize(new java.awt.Dimension(200, 35));
        settings.add(mdatabase_txt, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 292, -1, -1));

        reset_txt.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        reset_txt.setText("               Reset");
        reset_txt.setMaximumSize(new java.awt.Dimension(200, 35));
        reset_txt.setMinimumSize(new java.awt.Dimension(200, 35));
        reset_txt.setPreferredSize(new java.awt.Dimension(200, 35));
        settings.add(reset_txt, new org.netbeans.lib.awtextra.AbsoluteConstraints(708, 292, -1, -1));

        btnbdatabase.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnbdatabase.setMaximumSize(new java.awt.Dimension(200, 130));
        btnbdatabase.setMinimumSize(new java.awt.Dimension(200, 130));
        btnbdatabase.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnbdatabaseMouseClicked(evt);
            }
        });

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/icons8_book_shelf_filled_100px.png"))); // NOI18N

        javax.swing.GroupLayout btnbdatabaseLayout = new javax.swing.GroupLayout(btnbdatabase);
        btnbdatabase.setLayout(btnbdatabaseLayout);
        btnbdatabaseLayout.setHorizontalGroup(
            btnbdatabaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnbdatabaseLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(jLabel13)
                .addContainerGap(50, Short.MAX_VALUE))
        );
        btnbdatabaseLayout.setVerticalGroup(
            btnbdatabaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, btnbdatabaseLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
                .addContainerGap())
        );

        settings.add(btnbdatabase, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 156, -1, -1));

        btnmdatabase.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnmdatabase.setMaximumSize(new java.awt.Dimension(200, 130));
        btnmdatabase.setMinimumSize(new java.awt.Dimension(200, 130));
        btnmdatabase.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnmdatabaseMouseClicked(evt);
            }
        });

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/icons8_groups_100px.png"))); // NOI18N

        javax.swing.GroupLayout btnmdatabaseLayout = new javax.swing.GroupLayout(btnmdatabase);
        btnmdatabase.setLayout(btnmdatabaseLayout);
        btnmdatabaseLayout.setHorizontalGroup(
            btnmdatabaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnmdatabaseLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(jLabel14)
                .addContainerGap(50, Short.MAX_VALUE))
        );
        btnmdatabaseLayout.setVerticalGroup(
            btnmdatabaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnmdatabaseLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
                .addContainerGap())
        );

        settings.add(btnmdatabase, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 156, -1, -1));

        btnreset.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnreset.setMaximumSize(new java.awt.Dimension(200, 130));
        btnreset.setMinimumSize(new java.awt.Dimension(200, 130));
        btnreset.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnresetMouseClicked(evt);
            }
        });

        jLabel47.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/icons8_available_updates_100px_1.png"))); // NOI18N

        javax.swing.GroupLayout btnresetLayout = new javax.swing.GroupLayout(btnreset);
        btnreset.setLayout(btnresetLayout);
        btnresetLayout.setHorizontalGroup(
            btnresetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnresetLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(jLabel47)
                .addContainerGap(50, Short.MAX_VALUE))
        );
        btnresetLayout.setVerticalGroup(
            btnresetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnresetLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel47, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
                .addContainerGap())
        );

        settings.add(btnreset, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 156, -1, -1));

        btnback5.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnback5.setMaximumSize(new java.awt.Dimension(150, 45));
        btnback5.setMinimumSize(new java.awt.Dimension(150, 45));
        btnback5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnback5MouseClicked(evt);
            }
        });

        jLabel61.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel61.setText("BACK");

        javax.swing.GroupLayout btnback5Layout = new javax.swing.GroupLayout(btnback5);
        btnback5.setLayout(btnback5Layout);
        btnback5Layout.setHorizontalGroup(
            btnback5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnback5Layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jLabel61)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        btnback5Layout.setVerticalGroup(
            btnback5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnback5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel61, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        settings.add(btnback5, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 510, -1, -1));

        bseperator5.setBackground(new java.awt.Color(0, 0, 0));
        bseperator5.setForeground(new java.awt.Color(0, 0, 0));
        bseperator5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        settings.add(bseperator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 560, 1020, -1));
        settings.add(bbg5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        parent.add(settings, "card6");

        bookdatabase.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        bdatabase1_txt.setFont(new java.awt.Font("Californian FB", 0, 24)); // NOI18N
        bdatabase1_txt.setText("BOOK DATABASE");
        bookdatabase.add(bdatabase1_txt, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 20, -1, -1));

        btnback6.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnback6.setMaximumSize(new java.awt.Dimension(150, 45));
        btnback6.setMinimumSize(new java.awt.Dimension(150, 45));
        btnback6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnback6MouseClicked(evt);
            }
        });

        jLabel63.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel63.setText("BACK");

        javax.swing.GroupLayout btnback6Layout = new javax.swing.GroupLayout(btnback6);
        btnback6.setLayout(btnback6Layout);
        btnback6Layout.setHorizontalGroup(
            btnback6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnback6Layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jLabel63)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        btnback6Layout.setVerticalGroup(
            btnback6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnback6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel63, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        bookdatabase.add(btnback6, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 510, -1, -1));

        bseperator6.setBackground(new java.awt.Color(0, 0, 0));
        bseperator6.setForeground(new java.awt.Color(0, 0, 0));
        bseperator6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        bookdatabase.add(bseperator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 560, 1020, -1));
        bookdatabase.add(bbg6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        parent.add(bookdatabase, "card9");

        memberdatabase.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        mdatabase1_txt.setFont(new java.awt.Font("Californian FB", 0, 24)); // NOI18N
        mdatabase1_txt.setText("MEMBER DATABASE");
        memberdatabase.add(mdatabase1_txt, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 20, -1, -1));

        btnback7.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnback7.setMaximumSize(new java.awt.Dimension(150, 45));
        btnback7.setMinimumSize(new java.awt.Dimension(150, 45));
        btnback7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnback7MouseClicked(evt);
            }
        });

        jLabel68.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel68.setText("BACK");

        javax.swing.GroupLayout btnback7Layout = new javax.swing.GroupLayout(btnback7);
        btnback7.setLayout(btnback7Layout);
        btnback7Layout.setHorizontalGroup(
            btnback7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnback7Layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jLabel68)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        btnback7Layout.setVerticalGroup(
            btnback7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnback7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel68, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        memberdatabase.add(btnback7, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 510, -1, -1));

        bseperator7.setBackground(new java.awt.Color(0, 0, 0));
        bseperator7.setForeground(new java.awt.Color(0, 0, 0));
        bseperator7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        memberdatabase.add(bseperator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 560, 1020, -1));
        memberdatabase.add(bbg7, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        parent.add(memberdatabase, "card10");

        reset.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        reset1_txt.setFont(new java.awt.Font("Californian FB", 0, 24)); // NOI18N
        reset1_txt.setText("RESET");
        reset.add(reset1_txt, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 20, -1, -1));

        jPanel1.setBackground(new java.awt.Color(204, 204, 204, 120));

        changepw_txt.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        changepw_txt.setText("Reset Database");

        currentpw_txt.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        currentpw_txt.setText("Reset the members datebase.");

        newpw_txt.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        newpw_txt.setText("Reset the books database.");

        btnResetMembers.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnResetMembers.setMaximumSize(new java.awt.Dimension(150, 45));
        btnResetMembers.setMinimumSize(new java.awt.Dimension(150, 45));
        btnResetMembers.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnResetMembersMouseClicked(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel18.setText("Reset");

        javax.swing.GroupLayout btnResetMembersLayout = new javax.swing.GroupLayout(btnResetMembers);
        btnResetMembers.setLayout(btnResetMembersLayout);
        btnResetMembersLayout.setHorizontalGroup(
            btnResetMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnResetMembersLayout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jLabel18)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        btnResetMembersLayout.setVerticalGroup(
            btnResetMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnResetMembersLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnResetBooks.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnResetBooks.setMaximumSize(new java.awt.Dimension(150, 45));
        btnResetBooks.setMinimumSize(new java.awt.Dimension(150, 45));
        btnResetBooks.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnResetBooksMouseClicked(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel19.setText("Reset");

        javax.swing.GroupLayout btnResetBooksLayout = new javax.swing.GroupLayout(btnResetBooks);
        btnResetBooks.setLayout(btnResetBooksLayout);
        btnResetBooksLayout.setHorizontalGroup(
            btnResetBooksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnResetBooksLayout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jLabel19)
                .addContainerGap(46, Short.MAX_VALUE))
        );
        btnResetBooksLayout.setVerticalGroup(
            btnResetBooksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnResetBooksLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(448, 448, 448)
                .addComponent(changepw_txt)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(187, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(newpw_txt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(currentpw_txt))
                .addGap(145, 145, 145)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnResetBooks, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnResetMembers, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(238, 238, 238))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(changepw_txt)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnResetMembers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15)
                        .addComponent(btnResetBooks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(currentpw_txt, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(newpw_txt, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(40, Short.MAX_VALUE))
        );

        reset.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 280, 1020, 200));

        jPanel2.setBackground(new java.awt.Color(204, 204, 204, 120));

        changepw_txt1.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        changepw_txt1.setText("Change Password");

        currentpw_txt1.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        currentpw_txt1.setText("Enter Current Password:");

        newpw_txt1.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        newpw_txt1.setText("Enter New Password");

        btnChangePassword.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnChangePassword.setMaximumSize(new java.awt.Dimension(150, 45));
        btnChangePassword.setMinimumSize(new java.awt.Dimension(150, 45));
        btnChangePassword.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnChangePasswordMouseClicked(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel17.setText("Confirm");

        javax.swing.GroupLayout btnChangePasswordLayout = new javax.swing.GroupLayout(btnChangePassword);
        btnChangePassword.setLayout(btnChangePasswordLayout);
        btnChangePasswordLayout.setHorizontalGroup(
            btnChangePasswordLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnChangePasswordLayout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jLabel17)
                .addContainerGap(35, Short.MAX_VALUE))
        );
        btnChangePasswordLayout.setVerticalGroup(
            btnChangePasswordLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnChangePasswordLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        txtCurrentPassword.setMaximumSize(new java.awt.Dimension(215, 40));
        txtCurrentPassword.setMinimumSize(new java.awt.Dimension(215, 40));
        txtCurrentPassword.setPreferredSize(new java.awt.Dimension(215, 40));

        txtNewPassword.setMaximumSize(new java.awt.Dimension(215, 40));
        txtNewPassword.setMinimumSize(new java.awt.Dimension(215, 40));
        txtNewPassword.setPreferredSize(new java.awt.Dimension(215, 40));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(438, 438, 438)
                .addComponent(changepw_txt1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(241, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnChangePassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(newpw_txt1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(currentpw_txt1))
                        .addGap(124, 124, 124)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtCurrentPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtNewPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(238, 238, 238))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(changepw_txt1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(currentpw_txt1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCurrentPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(newpw_txt1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNewPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnChangePassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        reset.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 60, 1020, 200));

        btnback8.setBackground(new java.awt.Color(255, 153, 51, 100));
        btnback8.setMaximumSize(new java.awt.Dimension(150, 45));
        btnback8.setMinimumSize(new java.awt.Dimension(150, 45));
        btnback8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnback8MouseClicked(evt);
            }
        });

        jLabel69.setFont(new java.awt.Font("Californian FB", 1, 20)); // NOI18N
        jLabel69.setText("BACK");

        javax.swing.GroupLayout btnback8Layout = new javax.swing.GroupLayout(btnback8);
        btnback8.setLayout(btnback8Layout);
        btnback8Layout.setHorizontalGroup(
            btnback8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnback8Layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jLabel69)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        btnback8Layout.setVerticalGroup(
            btnback8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnback8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel69, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        reset.add(btnback8, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 510, -1, -1));

        bseperator8.setBackground(new java.awt.Color(0, 0, 0));
        bseperator8.setForeground(new java.awt.Color(0, 0, 0));
        bseperator8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        reset.add(bseperator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 560, 1020, -1));
        reset.add(bbg8, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        parent.add(reset, "card11");

        getContentPane().add(parent, java.awt.BorderLayout.CENTER);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnbtrackingMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnbtrackingMouseClicked
       parent.removeAll();
       parent.add(btmember);
       parent.repaint();
       parent.revalidate();
    }//GEN-LAST:event_btnbtrackingMouseClicked

    private void btnmbookMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnmbookMouseClicked
       parent.removeAll();
       parent.add(mbooks);
       parent.repaint();
       parent.revalidate();
    }//GEN-LAST:event_btnmbookMouseClicked

    private void btnmuserMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnmuserMouseClicked
       parent.removeAll();
       parent.add(musers);
       parent.repaint();
       parent.revalidate();
    }//GEN-LAST:event_btnmuserMouseClicked

    private void btnsettingMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnsettingMouseClicked
       parent.removeAll();
       parent.add(settings);
       parent.repaint();
       parent.revalidate();
    }//GEN-LAST:event_btnsettingMouseClicked

    private void btnlogoutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnlogoutMouseClicked
        // Confirm before comitting transaction.
        int response = JOptionPane
                .showConfirmDialog(this, 
                        "Really log out?", 
                        "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);       
       
        if (response == JOptionPane.NO_OPTION)
            return;
       
        AdminsDatabase.logOff();
        
        Login log = new Login();
        log.setVisible(true);
        log.pack();
        log.setLocationRelativeTo(null);
        log.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_btnlogoutMouseClicked

    private void btnbdatabaseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnbdatabaseMouseClicked
       parent.removeAll();
       parent.add(mbooks);
       parent.repaint();
       parent.revalidate();
    }//GEN-LAST:event_btnbdatabaseMouseClicked

    private void btnmdatabaseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnmdatabaseMouseClicked
       parent.removeAll();
       parent.add(musers);
       parent.repaint();
       parent.revalidate();
    }//GEN-LAST:event_btnmdatabaseMouseClicked

    private void btnresetMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnresetMouseClicked
       parent.removeAll();
       parent.add(reset);
       parent.repaint();
       parent.revalidate();
    }//GEN-LAST:event_btnresetMouseClicked

    private void btnbackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnbackMouseClicked
       parent.removeAll();
       parent.add(dash);
       parent.repaint();
       parent.revalidate();
    }//GEN-LAST:event_btnbackMouseClicked

    private void btnReturningMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnReturningMouseClicked
        // Don't do anything if more than one is selected, or none.
        if (tblBorrowersTrackingMembers.getSelectedRows().length != 1) {
            JOptionPane.showMessageDialog(null, "Please select a member.");
            return;
        }
        
        // Retrieve data to be put in the summary.
        int selected_row = tblBorrowersTrackingMembers.getSelectedRow();
        String member_id = tblBorrowersTrackingMembers.getValueAt(selected_row, 0).toString();

        // Show message if selected member has no borrowed books.
        if (MembersDatabase.getMember(member_id).bookCount() == 0) {
            JOptionPane.showMessageDialog(null, "This member has no borrowed books.");
            return;
        }
        
        String[][] selected_member_info_rows = {
            {
                MembersDatabase.getMember(member_id).getID(),
                MembersDatabase.getMember(member_id).getLastName(),
                MembersDatabase.getMember(member_id).getFirstName(),
                MembersDatabase.getMember(member_id).getSection(),
                MembersDatabase.getMember(member_id).getContactNumber()
            }
        };
        
        // Construct table data using column and rows array.
        TableModel selected_member_table_model = 
                new DefaultTableModel(selected_member_info_rows, 
                                      members_table_column_names) {
            @Override
            public boolean isCellEditable(int row, int column) {       
                return false; // or a condition at your choice with row and column
            }
        };
        
        // Record member info the summary page.
        tblReturningMemberSummary.setModel(selected_member_table_model);
        
        // Create array that will hold borrowed books.
        String[][] returnable_books_info_rows 
                = new String[MembersDatabase.getMember(member_id).bookCount()]
                            [books_table_column_names.length];
        
        int info_row_ctr = 0;
        for (String book_id : MembersDatabase.getMember(member_id).getBorrowedBookIDs()) {
            returnable_books_info_rows[info_row_ctr][0] = BooksDatabase.getBook(book_id).getID();
            returnable_books_info_rows[info_row_ctr][1] = BooksDatabase.getBook(book_id).getName();
            returnable_books_info_rows[info_row_ctr][2] = BooksDatabase.getBook(book_id).getAuthor();
            returnable_books_info_rows[info_row_ctr][3] = BooksDatabase.getBook(book_id).getPublisher();
            returnable_books_info_rows[info_row_ctr][4] = BooksDatabase.getBook(book_id).getDatePublished();
            
            info_row_ctr++;
        }
        
        // Construct table data using column and rows array.
        TableModel returnable_books_table_model = 
                new DefaultTableModel(returnable_books_info_rows, 
                                      books_table_column_names) {
            @Override
            public boolean isCellEditable(int row, int column) {       
                return false; // or a condition at your choice with row and column
            }
        };
        
        // Record member info the summary page.
        tblBorrowedBooks.setModel(returnable_books_table_model);
        
        // Switch card to Borrowers Tracking -> Returning.
        parent.removeAll();
        parent.add(btreturning);
        parent.repaint();
        parent.revalidate();
    }//GEN-LAST:event_btnReturningMouseClicked

    private void btnback1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnback1MouseClicked
       parent.removeAll();
       parent.add(btmember);
       parent.repaint();
       parent.revalidate();
    }//GEN-LAST:event_btnback1MouseClicked

    private void btnContinueReturningMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnContinueReturningMouseClicked
        // Show message if not book is selected.
        if (tblBorrowedBooks.getSelectedRows().length == 0) {
            JOptionPane.showMessageDialog(null, "No books selected.");
            return;
        }
        
        // Create array to hold selected book info.
        String[][] selected_books_info_rows 
                = new String[tblBorrowedBooks.getSelectedRows().length]
                            [books_table_column_names.length];
        
        // Transfer data from JTable object to array.
        int info_row_ctr = 0;
        for (int row_num : tblBorrowedBooks.getSelectedRows()) {
            selected_books_info_rows[info_row_ctr][0] = tblBorrowedBooks.getValueAt(row_num, 0).toString();
            selected_books_info_rows[info_row_ctr][1] = tblBorrowedBooks.getValueAt(row_num, 1).toString();
            selected_books_info_rows[info_row_ctr][2] = tblBorrowedBooks.getValueAt(row_num, 2).toString();
            selected_books_info_rows[info_row_ctr][3] = tblBorrowedBooks.getValueAt(row_num, 3).toString();
            selected_books_info_rows[info_row_ctr][4] = tblBorrowedBooks.getValueAt(row_num, 4).toString();
            
            info_row_ctr++;
        }
        
        // Construct table data using column and rows array.
        TableModel selected_books_table_model = 
                new DefaultTableModel(selected_books_info_rows, 
                                      books_table_column_names) {
            @Override
            public boolean isCellEditable(int row, int column) {       
                return false; // or a condition at your choice with row and column
            }
        };
        
        // Record member info the summary page.
        tblReturningBooksSummary.setModel(selected_books_table_model);
        
        parent.removeAll();
        parent.add(pnlBTReturningSummary);
        parent.repaint();
        parent.revalidate();
    }//GEN-LAST:event_btnContinueReturningMouseClicked

    private void btnback2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnback2MouseClicked
       parent.removeAll();
       parent.add(btmember);
       parent.repaint();
       parent.revalidate();
    }//GEN-LAST:event_btnback2MouseClicked

    private void btnBorrowOKMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBorrowOKMouseClicked
        // Confirm before comitting transaction.
        int response = JOptionPane
                .showConfirmDialog(this, 
                        "Confirm borrowing?", 
                        "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);       
        
        if (response == JOptionPane.NO_OPTION)
            return;
        
        String member_id = tblBorrowingMemberSummary.getValueAt(0, 0).toString();
        
        // Store previously selected books using the member ID.
        for (int row = 0; row < tblBorrowingBooksSummary.getRowCount(); row ++) {
            String book_id = tblBorrowingBooksSummary.getValueAt(row, 0).toString();
            MembersDatabase.getMember(member_id).addBook(book_id);
        }
        
        parent.removeAll();
        parent.add(dash);
        parent.repaint();
        parent.revalidate();
    }//GEN-LAST:event_btnBorrowOKMouseClicked

    private void btnback3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnback3MouseClicked
       parent.removeAll();
       parent.add(dash);
       parent.repaint();
       parent.revalidate();
    }//GEN-LAST:event_btnback3MouseClicked

    private void btnback4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnback4MouseClicked
       parent.removeAll();
       parent.add(dash);
       parent.repaint();
       parent.revalidate();
    }//GEN-LAST:event_btnback4MouseClicked

    private void btnback5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnback5MouseClicked
       parent.removeAll();
       parent.add(dash);
       parent.repaint();
       parent.revalidate();
    }//GEN-LAST:event_btnback5MouseClicked

    private void btnback6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnback6MouseClicked
       parent.removeAll();
       parent.add(settings);
       parent.repaint();
       parent.revalidate();
    }//GEN-LAST:event_btnback6MouseClicked

    private void btnback7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnback7MouseClicked
       parent.removeAll();
       parent.add(settings);
       parent.repaint();
       parent.revalidate();
    }//GEN-LAST:event_btnback7MouseClicked

    private void btnback8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnback8MouseClicked
       parent.removeAll();
       parent.add(settings);
       parent.repaint();
       parent.revalidate();
    }//GEN-LAST:event_btnback8MouseClicked

    private void btnAddBookMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddBookMouseClicked
        AddBook ai = new AddBook();
        ai.setVisible(true);
        ai.pack();
        ai.setLocationRelativeTo(null);
        ai.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_btnAddBookMouseClicked

    private void btnAddMemberMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddMemberMouseClicked
        AddMember ai = new AddMember();
        ai.setVisible(true);
        ai.pack();
        ai.setLocationRelativeTo(null);
        ai.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_btnAddMemberMouseClicked

    private void btnEditBookMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEditBookMouseClicked
        EditBook ai = new EditBook();
        ai.setVisible(true);
        ai.pack();
        ai.setLocationRelativeTo(null);
        ai.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.dispose();
    }//GEN-LAST:event_btnEditBookMouseClicked

    private void btnEditMemberMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEditMemberMouseClicked
        EditMember ai = new EditMember();
        ai.setVisible(true);
        ai.pack();
        ai.setLocationRelativeTo(null);
        ai.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.dispose();
    }//GEN-LAST:event_btnEditMemberMouseClicked

    private void btnBorrowingMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBorrowingMouseClicked
        // Don't do anything if more than one is selected, or none.
        if (tblBorrowersTrackingMembers.getSelectedRows().length != 1) {
            JOptionPane.showMessageDialog(null, "Please select a member.");
            return;
        }
        
        // Retrieve data to be put in the summary.
        int selected_row = tblBorrowersTrackingMembers.getSelectedRow();
        String member_id = tblBorrowersTrackingMembers.getValueAt(selected_row, 0).toString();

        String[][] selected_member_info_rows = {
            {
                MembersDatabase.getMember(member_id).getID(),
                MembersDatabase.getMember(member_id).getLastName(),
                MembersDatabase.getMember(member_id).getFirstName(),
                MembersDatabase.getMember(member_id).getSection(),
                MembersDatabase.getMember(member_id).getContactNumber()
            }
        };
        
        // Construct table data using column and rows array.
        TableModel selected_member_table_model = 
                new DefaultTableModel(selected_member_info_rows, 
                                      members_table_column_names) {
            @Override
            public boolean isCellEditable(int row, int column) {       
                return false; // or a condition at your choice with row and column
            }
        };
        
        // Record member info the summary page.
        tblBorrowingMemberSummary.setModel(selected_member_table_model);
        
        if (BooksDatabase.getAvailableBookIDs().size() > 0) {
            reloadAvailableBooksTable(tblAvailableBooks);
        }
        
        // Switch card to Borrowers Tracking -> Borrowing.
        parent.removeAll();
        parent.add(btborrowing);
        parent.repaint();
        parent.revalidate();
    }//GEN-LAST:event_btnBorrowingMouseClicked

    private void tblManageBooksMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblManageBooksMouseClicked

    }//GEN-LAST:event_tblManageBooksMouseClicked

    private void tblManageMembersMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblManageMembersMouseClicked
    
    }//GEN-LAST:event_tblManageMembersMouseClicked

    private void btnContinueBorrowMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnContinueBorrowMouseClicked
        // Show message if not book is selected.
        if (tblAvailableBooks.getSelectedRows().length == 0) {
            JOptionPane.showMessageDialog(null, "No books selected.");
            return;
        }
        
        // Create array to hold selected book info.
        String[][] selected_books_info_rows 
                = new String[tblAvailableBooks.getSelectedRows().length]
                            [books_table_column_names.length];
        
        // Transfer data from JTable object to array.
        int info_row_ctr = 0;
        for (int row_num : tblAvailableBooks.getSelectedRows()) {
            selected_books_info_rows[info_row_ctr][0] = tblAvailableBooks.getValueAt(row_num, 0).toString();
            selected_books_info_rows[info_row_ctr][1] = tblAvailableBooks.getValueAt(row_num, 1).toString();
            selected_books_info_rows[info_row_ctr][2] = tblAvailableBooks.getValueAt(row_num, 2).toString();
            selected_books_info_rows[info_row_ctr][3] = tblAvailableBooks.getValueAt(row_num, 3).toString();
            selected_books_info_rows[info_row_ctr][4] = tblAvailableBooks.getValueAt(row_num, 4).toString();
            
            info_row_ctr++;
        }
        
        // Construct table data using column and rows array.
        TableModel selected_books_table_model = 
                new DefaultTableModel(selected_books_info_rows, 
                                      books_table_column_names){
            @Override
            public boolean isCellEditable(int row, int column) {       
                return false; // or a condition at your choice with row and column
            }
        };
        
        // Record member info the summary page.
        tblBorrowingBooksSummary.setModel(selected_books_table_model);
        
        parent.removeAll();
        parent.add(btracking);
        parent.repaint();
        parent.revalidate();
    }//GEN-LAST:event_btnContinueBorrowMouseClicked

    private void btnback9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnback9MouseClicked
       parent.removeAll();
       parent.add(btmember);
       parent.repaint();
       parent.revalidate();
    }//GEN-LAST:event_btnback9MouseClicked

    private void txtBooksSearchFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBooksSearchFieldKeyTyped
        // Collect data from Swing object.
        String search_term = txtBooksSearchField.getText();
        
        reloadBooksTableWithFilter(tblManageBooks, search_term);
    }//GEN-LAST:event_txtBooksSearchFieldKeyTyped

    private void btnDeleteBookMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDeleteBookMouseClicked
        // Confirm before comitting transaction.
        int response = JOptionPane
                .showConfirmDialog(this, 
                        "Confirm delete?", 
                        "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);       
        
        if (response == JOptionPane.NO_OPTION)
            return;

        // Collect data from Swing object.
        int selected_row = tblManageBooks.getSelectedRow();
        
        if (selected_row > -1) {
            String book_id = tblManageBooks.getValueAt(selected_row, 0).toString();
            BooksDatabase.removeBook(book_id);
        }
           
        reloadBooksTable(tblManageBooks);
    }//GEN-LAST:event_btnDeleteBookMouseClicked

    private void txtMembersSearchFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMembersSearchFieldKeyTyped
        // Collect data from Swing object.
        String search_term = txtMembersSearchField.getText();
        
        reloadMembersTableWithFilter(tblManageMembers, search_term);
    }//GEN-LAST:event_txtMembersSearchFieldKeyTyped

    private void btnDeleteMemberMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDeleteMemberMouseClicked
        // Confirm before comitting transaction.
        int response = JOptionPane
                .showConfirmDialog(this, 
                        "Confirm delete?", 
                        "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);       
        
        if (response == JOptionPane.NO_OPTION)
            return;

        // Collect data from Swing object.
        int selected_row = tblManageMembers.getSelectedRow();
        
        if (selected_row > -1) {
            String member_id = tblManageMembers.getValueAt(selected_row, 0).toString();
            MembersDatabase.removeMember(member_id);
        }
           
        reloadMembersTable(tblManageMembers);
    }//GEN-LAST:event_btnDeleteMemberMouseClicked

    private void btnBackToChooseBooksToBorrowMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBackToChooseBooksToBorrowMouseClicked
        parent.removeAll();
        parent.add(btreturning);
        parent.repaint();
        parent.revalidate();
    }//GEN-LAST:event_btnBackToChooseBooksToBorrowMouseClicked

    private void btnReturnOKMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnReturnOKMouseClicked
        // Confirm before comitting transaction.
        int response = JOptionPane
                .showConfirmDialog(this, 
                        "Confirm returning?", 
                        "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);       
        
        if (response == JOptionPane.NO_OPTION)
            return;
        
        String member_id = tblReturningMemberSummary.getValueAt(0, 0).toString();
        
        // Store previously selected books using the member ID.
        for (int row = 0; row < tblReturningBooksSummary.getRowCount(); row ++) {
            String book_id = tblReturningBooksSummary.getValueAt(row, 0).toString();
            MembersDatabase.getMember(member_id).removeBook(book_id);
        }
        
        parent.removeAll();
        parent.add(dash);
        parent.repaint();
        parent.revalidate();
    }//GEN-LAST:event_btnReturnOKMouseClicked

    private void btnResetMembersMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnResetMembersMouseClicked
        // Confirm before resetting database.
        int response = JOptionPane
                .showConfirmDialog(this, 
                        "All members data will be LOST! Continue?", 
                        "Reset Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);       
        
        if (response == JOptionPane.NO_OPTION)
            return;
        
        MembersDatabase.init();
        // Clear Manage Members, Manage Users, Choose Member.
        DefaultTableModel dm1 = (DefaultTableModel) tblManageMembers.getModel();
        while(dm1.getRowCount() > 0) {
            dm1.removeRow(0);
        };
        
        DefaultTableModel dm2 = (DefaultTableModel) tblBorrowersTrackingMembers.getModel();
        while(dm2.getRowCount() > 0) {
            dm2.removeRow(0);
        };
    }//GEN-LAST:event_btnResetMembersMouseClicked

    private void btnResetBooksMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnResetBooksMouseClicked
        // Confirm before resetting database.
        int response = JOptionPane
                .showConfirmDialog(this, 
                        "All books data will be LOST! Continue?", 
                        "Reset Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);       
        
        if (response == JOptionPane.NO_OPTION)
            return;
        
        BooksDatabase.init();
        // Clear Manage Members, Manage Users, Choose Member.
        DefaultTableModel dm = (DefaultTableModel) tblManageBooks.getModel();
        while (dm.getRowCount() > 0) {
            dm.removeRow(0);
        };
    }//GEN-LAST:event_btnResetBooksMouseClicked

    private void btnChangePasswordMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnChangePasswordMouseClicked
        // Collect data from Swing objects.
        String current_password = String.valueOf(txtCurrentPassword.getPassword());
        String new_password = String.valueOf(txtNewPassword.getPassword());
        
        if (current_password.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Current password is empty.");
            return;
        }
        
        if (new_password.isEmpty()) {
            JOptionPane.showMessageDialog(null, "New password is empty.");
            return;
        }
        
        if (!AdminsDatabase
                .isCorrectPassword(AdminsDatabase
                        .getLoggedInUser(), current_password)) {
            JOptionPane.showMessageDialog(null, "Incorrect password.");
            return;
        }
            
        AdminsDatabase.changePassword(AdminsDatabase.getLoggedInUser(), new_password);
        AdminsDatabase.logOff();
        
        Login log = new Login();
        log.setVisible(true);
        log.pack();
        log.setLocationRelativeTo(null);
        log.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.dispose();
        
    }//GEN-LAST:event_btnChangePasswordMouseClicked

    private void reloadMembersTable(JTable table) {
        // Create an array that will store all the data in the table.
        String[][] members_table_rows = 
                new String[MembersDatabase.memberCount()]
                          [members_table_column_names.length];
        
        // Get the set of all Book IDs in the database, then
        // convert it into a LinkedList.
        Set<String> all_member_ids = MembersDatabase.getAllMemberIDs();
        
        LinkedList<String> members_table_book_keys = new LinkedList<>();
        for (String book_key : all_member_ids) {
            members_table_book_keys.add(book_key);
        }
        
        // Populate our array using the list of book keys.
        for (String[] row : members_table_rows) {
            // Assign values to each row based on the pre-defined column mapping.
            row[0] = MembersDatabase.getMember(members_table_book_keys.getFirst()).getID();
            row[1] = MembersDatabase.getMember(members_table_book_keys.getFirst()).getLastName();
            row[2] = MembersDatabase.getMember(members_table_book_keys.getFirst()).getFirstName();
            row[3] = MembersDatabase.getMember(members_table_book_keys.getFirst()).getSection();
            row[4] = MembersDatabase.getMember(members_table_book_keys.getFirst()).getContactNumber();
         
            // Remove item once finished.
            members_table_book_keys.removeFirst();
        }
        
        // Convert array into table data.
        TableModel books_table_model = new DefaultTableModel(members_table_rows, members_table_column_names){
            @Override
            public boolean isCellEditable(int row, int column) {       
                return false; // or a condition at your choice with row and column
            }
        };
        
        // Load table data into table object.
        table.setModel(books_table_model);
    }
    
    private void reloadBooksTable(JTable table) {
        // Create an array that will store all the data in the table.
        String[][] books_table_rows = 
                new String[BooksDatabase.bookCount()]
                          [books_table_column_names.length];
        
        // Get the set of all Book IDs in the database, then
        // convert it into a LinkedList.
        Set<String> all_book_ids = BooksDatabase.getAllBookIDs();
        
        LinkedList<String> books_table_book_keys = new LinkedList<>();
        for (String book_key : all_book_ids) {
            books_table_book_keys.add(book_key);
        }
        
        // Populate our array using the list of book keys.
        for (String[] row : books_table_rows) {
            // Assign values to each row based on the pre-defined column mapping.
            row[0] = BooksDatabase.getBook(books_table_book_keys.getFirst()).getID();
            row[1] = BooksDatabase.getBook(books_table_book_keys.getFirst()).getName();
            row[2] = BooksDatabase.getBook(books_table_book_keys.getFirst()).getAuthor();
            row[3] = BooksDatabase.getBook(books_table_book_keys.getFirst()).getPublisher();
            row[4] = BooksDatabase.getBook(books_table_book_keys.getFirst()).getDatePublished();
         
            // Remove item once finished.
            books_table_book_keys.removeFirst();
        }
        
        // Convert array into table data.
        TableModel books_table_model = new DefaultTableModel(books_table_rows, books_table_column_names){
            @Override
            public boolean isCellEditable(int row, int column) {       
                return false; // or a condition at your choice with row and column
            }
        };
        
        // Load table data into table object.
        table.setModel(books_table_model);
    }
    
    private void reloadBooksTableWithFilter(JTable table, String book_name_filter) {
        LinkedList<String> book_ids_matched_filter = new LinkedList<>();
        
        for (String book_id : BooksDatabase.getAllBookIDs()) {
            if (BooksDatabase
                .getBook(book_id).getName().toUpperCase()
                .contains(book_name_filter.toUpperCase())) {
                book_ids_matched_filter.add(book_id);
            }
        }
        
        if (book_ids_matched_filter.isEmpty()) {
            DefaultTableModel dm = (DefaultTableModel) table.getModel();
            while(dm.getRowCount() > 0) {
                dm.removeRow(0);
            }
            
            return;
        }
        
        // Create an array that will store all the matching data in the table.
        String[][] books_table_rows = 
                new String[book_ids_matched_filter.size()]
                          [books_table_column_names.length];
        
        // Populate our array using the list of book keys.
        for (String[] row : books_table_rows) {
            // Assign values to each row based on the pre-defined column mapping.
            row[0] = BooksDatabase.getBook(book_ids_matched_filter.getFirst()).getID();
            row[1] = BooksDatabase.getBook(book_ids_matched_filter.getFirst()).getName();
            row[2] = BooksDatabase.getBook(book_ids_matched_filter.getFirst()).getAuthor();
            row[3] = BooksDatabase.getBook(book_ids_matched_filter.getFirst()).getPublisher();
            row[4] = BooksDatabase.getBook(book_ids_matched_filter.getFirst()).getDatePublished();
        
            // Remove item once finished.
            book_ids_matched_filter.removeFirst();
        }
        
        // Convert array into table data.
        TableModel books_table_model = new DefaultTableModel(books_table_rows, books_table_column_names) {
            @Override
            public boolean isCellEditable(int row, int column) {       
                return false; // or a condition at your choice with row and column
            }
        };
        
        // Load table data into table object.
        table.setModel(books_table_model);
    }
    
    private void reloadMembersTableWithFilter(JTable table, String member_name_filter) {
        LinkedList<String> member_ids_matched_filter = new LinkedList<>();
        
        for (String member_id : MembersDatabase.getAllMemberIDs()) {
            if (MembersDatabase
                .getMember(member_id).getName().toUpperCase()
                .contains(member_name_filter.toUpperCase())) {
                member_ids_matched_filter.add(member_id);
            }
        }
        
        if (member_ids_matched_filter.isEmpty()) {
            DefaultTableModel dm = (DefaultTableModel) table.getModel();
            while(dm.getRowCount() > 0) {
                dm.removeRow(0);
            }
            
            return;
        }
        
        // Create an array that will store all the matching data in the table.
        String[][] members_table_rows = 
                new String[member_ids_matched_filter.size()]
                          [members_table_column_names.length];
        
        // Populate our array using the list of book keys.
        for (String[] row : members_table_rows) {
            // Assign values to each row based on the pre-defined column mapping.
            row[0] = MembersDatabase.getMember(member_ids_matched_filter.getFirst()).getID();
            row[1] = MembersDatabase.getMember(member_ids_matched_filter.getFirst()).getLastName();
            row[2] = MembersDatabase.getMember(member_ids_matched_filter.getFirst()).getFirstName();
            row[3] = MembersDatabase.getMember(member_ids_matched_filter.getFirst()).getSection();
            row[4] = MembersDatabase.getMember(member_ids_matched_filter.getFirst()).getContactNumber();
        
            // Remove item once finished.
            member_ids_matched_filter.removeFirst();
        }
        
        // Convert array into table data.
        TableModel members_table_model = new DefaultTableModel(members_table_rows, members_table_column_names) {
            @Override
            public boolean isCellEditable(int row, int column) {       
                return false; // or a condition at your choice with row and column
            }
        };
        
        // Load table data into table object.
        table.setModel(members_table_model);
    }
   
    private void reloadBorrowedBooksTable(JTable table, String member_id) {
        // Create an array that will store all the data in the table.
        String[][] books_table_rows = 
                new String[MembersDatabase.getMember(member_id).getBorrowedBookIDs().size()]
                          [books_table_column_names.length];
        
        // Get the set of borrowed Book IDs in the database, then
        // convert it into a LinkedList.
        List<String> borrowed_book_ids = MembersDatabase.getMember(member_id).getBorrowedBookIDs();
        
        LinkedList<String> books_table_book_keys = new LinkedList<>(borrowed_book_ids);
        
        // Populate our array using the list of book keys.
        for (String[] row : books_table_rows) {
            // Assign values to each row based on the pre-defined column mapping.
            row[0] = BooksDatabase.getBook(books_table_book_keys.getFirst()).getID();
            row[1] = BooksDatabase.getBook(books_table_book_keys.getFirst()).getName();
            row[2] = BooksDatabase.getBook(books_table_book_keys.getFirst()).getAuthor();
            row[3] = BooksDatabase.getBook(books_table_book_keys.getFirst()).getPublisher();
            row[4] = BooksDatabase.getBook(books_table_book_keys.getFirst()).getDatePublished();
         
            // Remove item once finished.
            books_table_book_keys.removeFirst();
        }
        
        // Convert array into table data.
        TableModel books_table_model = new DefaultTableModel(books_table_rows, books_table_column_names) {
            @Override
            public boolean isCellEditable(int row, int column) {       
                return false; // or a condition at your choice with row and column
            }
        };
        
        // Load table data into table object.
        table.setModel(books_table_model);
    }
    
    private void reloadAvailableBooksTable(JTable table) {
        // Create an array that will store all the data in the table.
        String[][] books_table_rows = 
                new String[BooksDatabase.getAvailableBookIDs().size()]
                          [books_table_column_names.length];
        
        // Get the set of borrowed Book IDs in the database, then
        // convert it into a LinkedList.
        ArrayList<String> available_book_ids = BooksDatabase.getAvailableBookIDs();
        
        LinkedList<String> books_table_book_keys = new LinkedList<>();
        for (String book_id : available_book_ids) {
            books_table_book_keys.add(book_id);
        }

        // Populate our array using the list of book keys.
        for (String[] row : books_table_rows) {
            // Assign values to each row based on the pre-defined column mapping.
            row[0] = BooksDatabase.getBook(books_table_book_keys.getFirst()).getID();
            row[1] = BooksDatabase.getBook(books_table_book_keys.getFirst()).getName();
            row[2] = BooksDatabase.getBook(books_table_book_keys.getFirst()).getAuthor();
            row[3] = BooksDatabase.getBook(books_table_book_keys.getFirst()).getPublisher();
            row[4] = BooksDatabase.getBook(books_table_book_keys.getFirst()).getDatePublished();
         
            // Remove item once finished.
            books_table_book_keys.removeFirst();
        }
        
        // Convert array into table data.
        TableModel books_table_model = new DefaultTableModel(books_table_rows, books_table_column_names) {
            @Override
            public boolean isCellEditable(int row, int column) {       
                return false; // or a condition at your choice with row and column
            }
        };
        
        // Load table data into table object.
        table.setModel(books_table_model);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Dashboard.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Dashboard.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Dashboard.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Dashboard.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Dashboard().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel background1;
    private javax.swing.JLabel background2;
    private javax.swing.JLabel background3;
    private javax.swing.JLabel background4;
    private javax.swing.JLabel bbg;
    private javax.swing.JLabel bbg1;
    private javax.swing.JLabel bbg10;
    private javax.swing.JLabel bbg2;
    private javax.swing.JLabel bbg3;
    private javax.swing.JLabel bbg4;
    private javax.swing.JLabel bbg5;
    private javax.swing.JLabel bbg6;
    private javax.swing.JLabel bbg7;
    private javax.swing.JLabel bbg8;
    private javax.swing.JLabel bbg9;
    private javax.swing.JLabel bdatabase1_txt;
    private javax.swing.JLabel bdatabase_txt;
    private javax.swing.JPanel bookdatabase;
    private javax.swing.JLabel books1;
    private javax.swing.JLabel books_txt1;
    private javax.swing.JScrollPane booktable;
    private javax.swing.JScrollPane booktable1;
    private javax.swing.JScrollPane booktable2;
    private javax.swing.JScrollPane booktable3;
    private javax.swing.JSeparator bseperator;
    private javax.swing.JSeparator bseperator1;
    private javax.swing.JSeparator bseperator10;
    private javax.swing.JSeparator bseperator2;
    private javax.swing.JSeparator bseperator3;
    private javax.swing.JSeparator bseperator4;
    private javax.swing.JSeparator bseperator5;
    private javax.swing.JSeparator bseperator6;
    private javax.swing.JSeparator bseperator7;
    private javax.swing.JSeparator bseperator8;
    private javax.swing.JSeparator bseperator9;
    private javax.swing.JPanel btborrowing;
    private javax.swing.JPanel btmember;
    private javax.swing.JPanel btnAddBook;
    private javax.swing.JPanel btnAddMember;
    private javax.swing.JPanel btnBackToChooseBooksToBorrow;
    private javax.swing.JPanel btnBorrowOK;
    private javax.swing.JPanel btnBorrowing;
    private javax.swing.JPanel btnChangePassword;
    private javax.swing.JPanel btnContinueBorrow;
    private javax.swing.JPanel btnContinueReturning;
    private javax.swing.JPanel btnDeleteBook;
    private javax.swing.JPanel btnDeleteMember;
    private javax.swing.JPanel btnEditBook;
    private javax.swing.JPanel btnEditMember;
    private javax.swing.JPanel btnResetBooks;
    private javax.swing.JPanel btnResetMembers;
    private javax.swing.JPanel btnReturnOK;
    private javax.swing.JPanel btnReturning;
    private javax.swing.JPanel btnback;
    private javax.swing.JPanel btnback1;
    private javax.swing.JPanel btnback2;
    private javax.swing.JPanel btnback3;
    private javax.swing.JPanel btnback4;
    private javax.swing.JPanel btnback5;
    private javax.swing.JPanel btnback6;
    private javax.swing.JPanel btnback7;
    private javax.swing.JPanel btnback8;
    private javax.swing.JPanel btnback9;
    private javax.swing.JPanel btnbdatabase;
    private javax.swing.JPanel btnbtracking;
    private javax.swing.JPanel btnlogout;
    private javax.swing.JPanel btnmbook;
    private javax.swing.JPanel btnmdatabase;
    private javax.swing.JPanel btnmuser;
    private javax.swing.JPanel btnreset;
    private javax.swing.JPanel btnsearch1;
    private javax.swing.JPanel btnsearch2;
    private javax.swing.JPanel btnsearch3;
    private javax.swing.JPanel btnsetting;
    private javax.swing.JLabel btnsummary;
    private javax.swing.JLabel btnsummary1;
    private javax.swing.JLabel btnsummary2;
    private javax.swing.JPanel btracking;
    private javax.swing.JLabel btracking1_txt;
    private javax.swing.JLabel btracking1_txt1;
    private javax.swing.JLabel btracking2_txt;
    private javax.swing.JLabel btracking2_txt1;
    private javax.swing.JLabel btracking_txt;
    private javax.swing.JPanel btreturning;
    private javax.swing.JLabel changepw_txt;
    private javax.swing.JLabel changepw_txt1;
    private javax.swing.JLabel currentpw_txt;
    private javax.swing.JLabel currentpw_txt1;
    private javax.swing.JPanel dash;
    private javax.swing.JLabel dashBG;
    private javax.swing.JLabel date;
    private javax.swing.JLabel icon_Mbook;
    private javax.swing.JLabel icon_Muser;
    private javax.swing.JLabel icon_Settings;
    private javax.swing.JLabel icon_Tracking;
    private javax.swing.JLabel icon_logout;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblProgramName;
    private javax.swing.JLabel lblWelcome;
    private javax.swing.JPanel mbooks;
    private javax.swing.JLabel mbooks_txt;
    private javax.swing.JLabel mdatabase1_txt;
    private javax.swing.JLabel mdatabase_txt;
    private javax.swing.JPanel memberdatabase;
    private javax.swing.JLabel members_txt;
    private javax.swing.JScrollPane membertable;
    private javax.swing.JScrollPane membertable1;
    private javax.swing.JScrollPane membertable2;
    private javax.swing.JScrollPane mmembertable;
    private javax.swing.JScrollPane mmembertable1;
    private javax.swing.JLabel muser_txt;
    private javax.swing.JPanel musers;
    private javax.swing.JLabel newpw_txt;
    private javax.swing.JLabel newpw_txt1;
    private javax.swing.JPanel parent;
    private javax.swing.JPanel pnlBTReturningSummary;
    private javax.swing.JPanel reset;
    private javax.swing.JLabel reset1_txt;
    private javax.swing.JLabel reset_txt;
    private javax.swing.JLabel returning_txt;
    private javax.swing.JLabel setting_txt;
    private javax.swing.JPanel settings;
    private javax.swing.JTable tblAvailableBooks;
    private javax.swing.JTable tblBorrowedBooks;
    private javax.swing.JTable tblBorrowersTrackingMembers;
    private javax.swing.JTable tblBorrowingBooksSummary;
    private javax.swing.JTable tblBorrowingMemberSummary;
    private javax.swing.JTable tblManageBooks;
    private javax.swing.JTable tblManageMembers;
    private javax.swing.JTable tblReturningBooksSummary;
    private javax.swing.JTable tblReturningMemberSummary;
    private javax.swing.JLabel time;
    private javax.swing.JTextField txtBooksSearchField;
    private javax.swing.JPasswordField txtCurrentPassword;
    private javax.swing.JTextField txtMembersSearchField;
    private javax.swing.JPasswordField txtNewPassword;
    private javax.swing.JLabel user_txt;
    // End of variables declaration//GEN-END:variables

    
}
