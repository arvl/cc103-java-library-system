/*
 * The MIT License
 *
 * Copyright 2021 Angelo Gabriel A. Geulin, David Allen Laud, 
 * Ahbby Ghelle A. Tinay, Anne Clarisse H. Gonzales, Kevin Bryan Y. Malabag
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package library;

/**
 *
 * @author arvl
 */
public class Book extends IdentifiableItem {
    public Book(String book_id, String book_name, String author, String publisher, String date_published) {
        super(book_id, book_name);
        this.author = author;
        this.publisher = publisher;
        this.date_published = date_published;
    }
    
    public String getAuthor() {
        return author;
    }
    
    public String getPublisher() {
        return publisher;
    }
    
    public String getDatePublished() {
        return date_published;
    }
    
    private final String author;
    private final String publisher;
    private final String date_published;
}
